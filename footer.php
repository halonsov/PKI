<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

?>
                    </div>
                </section>
                <div class="small-12 large-8 large-centered columns">
                    <?= $_SESSION['errors']->errorsGet();?>
                </div>
                <div class="newsletter">
                    <footer class="full-width" role="contentinfo">
                        <div class="row">
                            <div class="small-12 large-12 large-centered columns">   
                                <div class="large-12 small-12 columns">
                                    <div class="large-12 small-12 columns">
                                        <ul class="inline-list">
                                            <li>
                                                <a href="/">Inicio</a>
                                            </li>
                                            <li>
                                                <a href="">Contacto</a>
                                            </li>
                                            <li>
                                                <a href="/registration">Registro</a>
                                            </li>
                                            <li>
                                                <a href="">Términos y Condiciones</a>
                                            </li>
                                            <li>
                                                <a href="">Aviso de Privacidad.</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <script>
                        $(document).foundation();
                        $(document).foundation().foundation('joyride', 'start');
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>