<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;
use Exception;
use DateTime;
use ErrorException;
use PHPMailer;
use finfo;

/**
 * Clase que contiene funciones generales del sitio
 */
class Functions
{

    /**
	 * Contructor por defecto
	 *
	 * @method void _construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
    public function __construct() {}

    /**
	 * Elimina todas las $key numéricas del array $array[$key]
	 *
	 * @method array fetchUnset() Elimina todas las $key numéricas del array $array[$key]
	 * @access public
	 * @param array $fetch
	 * @return array
	 * @example  $this->fetchUnset(array( 'key' => 'value', '2' => 'value2')) regresa array('key' => 'value') 
	 */
    public function fetchUnset($fetch)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->fetchUnset($fetch) : '.__FILE__.' ('.__LINE__.')');
        if( !isset($fetch) OR !is_array($fetch) OR count($fetch) == 0)
        {
            $_SESSION['errors']->errorAdd('Se esperaba un array $this->fetchUnset($fetch) no nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        foreach($fetch AS $key => $value)
        {
            try {
                if(is_numeric($key))
                {
                    unset($fetch[$key]);
                }
            } catch(Exception $e) {
                $_SESSION['errors']->errorAdd('Ocurrió un error '.$e->getMessage().' : '.__FILE__.' ('.__LINE__.')');
            }
        }

        return $fetch;
    }

    /**
	 * Elimina todas las $key1 numéricas del array $array[$key][$key1]
	 *
	 * @method array fetchUnset() Elimina todas las $key1 numéricas del array
	 * @access public
	 * @param array $fetchAll
	 * @return array
	 */
    public function fetchAllUnset($fetchAll)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->fetchAllUnset($fetch) : '.__FILE__.' ('.__LINE__.')');
        if( !isset($fetchAll) OR !is_array($fetchAll) OR count($fetchAll) == 0)
        {
            $_SESSION['errors']->errorAdd('Se esperaba un array $this->fetchAllUnset($fetchAll) no nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        foreach($fetchAll AS $key => $value)
		{
            if( !is_array($value) OR count($value) == 0)
            {
                $_SESSION['errors']->errorAdd('Se esperaba un array $value no nulo : '.__FILE__.' ('.__LINE__.')');
            }

			foreach($value AS $key1 => $value1)
			{
                try {
                    if(is_numeric($key1))
                    {
                        unset($fetchAll[$key][$key1]);
                    }
                } catch (Exeption $e) {
                    $_SESSION['errors']->errorAdd('Ocurrió un error '.$e->getMessage().' : '.__FILE__.' ('.__LINE__.')');
                }
			}
		}

        return $fetchAll;
    }

    /**
	 * Convierte un array a options de select html
	 *
	 * @method string arrayOptionSet() Convierte un array a options de select html
	 * @access public
	 * @param array $array
	 * @return string
	 * @example $this->arrayOptionSet(array('key1' => 'value1', 'key2' => 'value2')) regresa <option value='key1'>value1</option><option value='key2'>value2</option>
	 */
    public function arrayOptionSet($array)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->arrayOptionSet($array) : '.__FILE__.' ('.__LINE__.')');
        $option = '';
        if(is_array($array) AND count($array) > 0)
        {
            foreach($array AS $key => $value)
            {
                if($key == '' OR $value == '')
                {
                    $_SESSION['errors']->errorAdd('$key nulo o $value nulo : '.__FILE__.' ('.__LINE__.')');
                    continue;
                } else {
                    $option .= "<option value='$key'>$value</option>";
                }
            }
            return $option;
        } else {
            $_SESSION['errors']->errorAdd('Se esperaba un array $this->arrayOptionSet($array) : '.__FILE__.' ('.__LINE__.')');
            return $option;
        }
    }

    /**
	 * Convierte un array a string, espaciando cada valor de cada entrada por una coma
	 *
	 * @method string arrayAutoloadSet() Convierte un array a string, espaciando cada valor de cada entrada por una coma
	 * @access public
	 * @param array $array
	 * @return string
	 * @example $this->arrayAutoloadSet(array('key1' => 'value1', 'key2' => 'value2')) regresa value1,value2
	 */
    public function arrayAutoloadSet($array)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->arrayAutoloadSet($array) : '.__FILE__.' ('.__LINE__.')');
        $autoload = '';
        if(is_array($array) AND count($array) > 0)
        {
            $i=0;
            foreach($array AS $key => $value)
            {
                if($key == '' OR $value == '')
                {
                    $_SESSION['errors']->errorAdd('$key nulo o $value nulo : '.__FILE__.' ('.__LINE__.')');
                    continue;
                } else {
                    if($i == 0)
                    {
                        $autoload .= '"'.$value.'"';
                        $i++;
                    } else {
                        $autoload .= ',"'.$value.'"';
                    }
                }
            }
            return $autoload;
        } else {
            $_SESSION['errors']->errorAdd('Se esperaba un array $this->arrayAutoloadSet($array) : '.__FILE__.' ('.__LINE__.')');
            return $autoload;
        }
    }

    /**
	 * Valida "tipos" de dato por expreciones regulares
	 *
	 * @method boolean expressionValidate() Valida "tipos" de dato por expreciones regulares
	 * @access public
	 * @param $type
	 * @param $expression
	 * @return string
	 */
    public function expressionValidate($type, $expression)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->expressionValidate($type, $expression) : '.__FILE__.' ('.__LINE__.')');
        switch($type)
        {
            case 'email':
                if( preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$expression) )
                {
                    return true;
                } else {
                    $_SESSION['errors']->errorAdd($expression.' no cumple la expresión regular ('.$type.') : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }

            case 'alpha':
                if( preg_match("/^([a-zA-ZáéíóúàèìòùâêîôûäëïöüñÀÈÌÒÙÁÉÍÓÚÂÊÎÔÛÄËÏÖÜÑ[:space:]])+$/",$expression) )
                {
                    return true;
                } else {
                    $_SESSION['errors']->errorAdd($expression.' no cumple la expresión regular ('.$type.') : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }

            case 'date':
                try {
                    $date = new DateTime($expression);
                    unset($date);
                    return true;
                } catch ( Exception $e ) {
                    $_SESSION['errors']->errorAdd($expression.' no es una fecha valida '.$e->getMessage().' : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }

            case 'usser':
                if( preg_match("/^([a-zA-Z0-9_])+$/",$expression) )
                {
                    return true;
                } else {
                    $_SESSION['errors']->errorAdd($expression.' no cumple la expresión regular ('.$type.') : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }

            case 'mail':
                if ( preg_match('/^[^0-9][a-zA-Z0-9._-]*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $expression) )
                {
                    return true;
                } else {
                    $_SESSION['errors']->errorAdd($expression.' no cumple la expresión regular ('.$type.') : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }

			case 'module':
				if( preg_match('/^[[:alpha:]]+Module$/', $expression) )
				{
					return true;
                } else {
                    $_SESSION['errors']->errorAdd($expression.' no cumple la expresión regular ('.$type.') : '.__FILE__.' ('.__LINE__.')');
                    return false;
                }
        }
    }

    /**
	 * Hace una petición a un sitio por medio de curl
	 *
	 * @method string curlGet() Hace una petición a un sitio por medio de curl
	 * @access public
	 * @param string $url
	 * @param array $options
	 * @return mixed
	 */
    public function curlGet($url = FALSE, $options = FALSE)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->curlGet() : '.__FILE__.' ('.__LINE__.')');
        if( !isset($url) OR $url == '')
        {
            return false;
        }

        if( !isset($options['return']) OR $options['return'] == '' OR ($options['return'] != 1 AND $options['return'] != 0) )
        {
            $options['return'] = 1;
        }

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $url);

        if( isset($options['ssl']) AND $options['ssl'] == true OR $options['ssl'] == 1)
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        } else {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }

        if( isset($options['post']) AND is_array($options['post']) )
        {
            curl_setopt($curl, CURLOPT_POST, count($options['post']));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $options['post']);	
        }

        if( isset($options['htmlheaders']) AND ($options['htmlheaders'] == TRUE OR $options['htmlheaders'] == 1) )
        {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, $options['return']);
        return curl_exec($curl);
    }

    /**
	 * Envía correo, usa phpmailer
	 *
	 * @method string mailSend() Envía correo, usa phpmailer
	 * @access public
	 * @param array $type
	 * @return string
	 */
    public function mailSend($array = FALSE)
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->mailSend() : '.__FILE__.' ('.__LINE__.')');
        if( !isset($array) OR !is_array($array))
        {
            $_SESSION['errors']->errorAdd('Se esperaba un array $this->mailSend($array) : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        $_SESSION['errors']->otherAdd('Creando objeto new PHPMailer() : '.__FILE__.' ('.__LINE__.')');
        $mail = new PHPMailer();
        $mail->IsSMTP();

        if(!isset($array['SMTPAuth']))
        {
            $_SESSION['errors']->warningAdd('No está definido $array[\'SMTPAuth\'] : '.__FILE__.' ('.__LINE__.')');
            $mail->SMTPAuth = TRUE;
        } else {
            $mail->SMTPAuth= $array['SMTPAuth'];
        }

        if( isset($array['SMTPDebug']) AND ( $array['SMTPDebug'] == 1 OR $array['SMTPDebug'] == 2 ) )
        {
            $mail->SMTPDebug = $array['SMTPDebug'];    // enables SMTP debug information (for testing)
        }

        if( isset($array['SMTPSecure']) )
        {
            $mail->SMTPSecure = $array['SMTPSecure'];      // sets the prefix to the servier
        } else {
            $_SESSION['errors']->warningAdd('No está definido $array[\'SMTPSecure\'] : '.__FILE__.' ('.__LINE__.')');
            $mail->SMTPSecure = DEFAULT_SMTP_SECURE;
        }

        if( isset($array['Host']) )
        {
            $mail->Host = $array['Host'];     // SMTP server
        } else {
            $_SESSION['errors']->warningAdd('No está definido $array[\'Host\'] : '.__FILE__.' ('.__LINE__.')');
            $mail->Host = DEFAULT_SMTP_HOST;
        }

        if( isset($array['Port']) )
        {
            $mail->Port = $array['Port'];
        } else {
            $_SESSION['errors']->warningAdd('No está definido $array[\'Port\'] : '.__FILE__.' ('.__LINE__.')');
            $mail->Port = DEFAUL_SMTP_PORT;
        }

        if( !isset($array['From']) OR !is_array($array['From']) )
        {
            $_SESSION['errors']->warningAdd('No está definido $array[\'From\'] : '.__FILE__.' ('.__LINE__.')');
            $mail->Username = DEFAULT_SMTP_USSER;
            $mail->Password = DEFAULT_SMTP_PASSWORD;
            $mail->SetFrom(DEFAULT_SMTP_FROM_MAIL, DEFAULT_SMTP_FROM_NAME);
        } else {
            if( isset($array['From']['Username']) )
            {
                $mail->Username = $array['From']['Username']; // username
            } else {
                $mail->Username = DEFAULT_SMTP_USSER;
            }
    
            if( isset($array['From']["Password"]) )
            {
                $mail->Password = $array['From']["Password"];    // password
            } else {
                $mail->Password = DEFAULT_SMTP_PASSWORD;
            }

            if( isset($array['From']["From"]) )
            {
                $fMail = $array['From']["From"];
            } else {
                $fMail = DEFAULT_SMTP_FROM_MAIL;
            }

            if( isset($array['From']['Name']) )
            {
                $fName = $array['From']["Name"];
            } else {
                $fName = DEFAULT_SMTP_FROM_NAME;
            }
            $mail->SetFrom($fMail, $fName);
        }

        if( !isset($array['Address']) )
        {
            $_SESSION['errors']->errorAdd('No está definido $array[\'Address\'] : '.__FILE__.' ('.__LINE__.')');
            return false;
        } else {
            if( is_array($array['Address']) )
            {
                if( !isset($array['Address']['Address']) )
                {
                    foreach( $array['Address'] AS $value)
                    {
                        if( !is_array($value) )
                        {
                            if($this->expressionValidate('mail', $value))
                            {
                                $mail->AddAddress($value);
                            } else {
                                $_SESSION['errors']->errorAdd($value.' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                            }
                        }
                    }
                } else {
                    if( !is_array($array['Address']['Address']) )
                    {
                        $mail->AddAddress($array['Address']['Address']);
                    } else {
                        foreach($array['Address']['Address'] AS $value)
                        {
                            if( !is_array($value) )
                            {
                                if($this->expressionValidate('mail', $value))
                                {
                                    $mail->AddAddress($value);
                                } else {
                                    $_SESSION['errors']->errorAdd($value.' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                }
                            } elseif( !isset($value['mail']) ) {
                                $_SESSION['errors']->errorAdd('No está definido $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                continue;
                            }elseif( is_array($value['mail']) ) {
                                $_SESSION['errors']->errorAdd('No se espera array $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                continue;
                            } elseif( !isset($value['name']) OR is_array($value['name']) ) {
                                if($this->expressionValidate('mail', $value['mail']))
                                {
                                    $mail->AddAddress($value['mail']);
                                } else {
                                    $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                }
                            } else {
                                if($this->expressionValidate('mail', $value['mail']))
                                {
                                    $mail->AddAddress($value['mail'], $value['name']);
                                } else {
                                    $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                }
                            }
                        }
                    }

                    if( isset($array['Address']['ReplyTo']) )
                    {
                        if( !is_array($array['Address']['ReplyTo']) )
                        {
                            if($this->expressionValidate('mail', $array['Address']['ReplyTo']))
                            {
                                $mail->addReplyTo($array['Address']['ReplyTo']);
                            } else {
                                $_SESSION['errors']->errorAdd($array['Address']['ReplyTo'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                            }
                        } else {
                            foreach($array['Address']['ReplyTo'] AS $value)
                            {
                                if( !is_array($value) )
                                {
                                    if($this->expressionValidate('mail', $value))
                                    {
                                        $mail->addReplyTo($value);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value.' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } elseif( !isset($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No está definido $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                }elseif( is_array($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No se espera array $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                } elseif( !isset($value['name']) OR is_array($value['name']) ) {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addReplyTo($value['mail']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } else {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addReplyTo($value['mail'], $value['name']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                }
                            }
                        }
                    }

                    if( isset($array['Address']['CC']) )
                    {
                        if( !is_array($array['Address']['CC']) )
                        {
                            if($this->expressionValidate('mail', $array['Address']['CC']))
                            {
                                $mail->addCC($array['Address']['CC']);
                            } else {
                                $_SESSION['errors']->errorAdd($array['Address']['CC'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                            }
                        } else {
                            foreach($array['Address']['CC'] AS $value)
                            {
                                if( !is_array($value) )
                                {
                                    if($this->expressionValidate('mail', $value))
                                    {
                                        $mail->addCC($value);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value.' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } elseif( !isset($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No está definido $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                }elseif( is_array($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No se espera array $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                } elseif( !isset($value['name']) OR is_array($value['name']) ) {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addCC($value['mail']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } else {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addCC($value['mail'], $value['name']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                }
                            }
                        }
                    }

                    if( isset($array['Address']['BCC']) )
                    {
                        if( !is_array($array['Address']['BCC']) )
                        {
                            if($this->expressionValidate('mail', $array['Address']['BCC']))
                            {
                                $mail->addBCC($array['Address']['BCC']);
                            } else {
                                $_SESSION['errors']->errorAdd($array['Address']['BCC'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                            }
                        } else {
                            foreach($array['Address']['BCC'] AS $value)
                            {
                                if( !is_array($value) )
                                {
                                    if($this->expressionValidate('mail', $value))
                                    {
                                        $mail->addBCC($value);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value.' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } elseif( !isset($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No está definido $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                }elseif( is_array($value['mail']) ) {
                                    $_SESSION['errors']->errorAdd('No se espera array $value[\'mail\'] : '.__FILE__.' ('.__LINE__.')');
                                    continue;
                                } elseif( !isset($value['name']) OR is_array($value['name']) ) {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addBCC($value['mail']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                } else {
                                    if($this->expressionValidate('mail', $value['mail']))
                                    {
                                        $mail->addBCC($value['mail'], $value['name']);
                                    } else {
                                        $_SESSION['errors']->errorAdd($value['mail'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if($this->expressionValidate('mail', $array['Address'])) {
                    $mail->AddAddress($array['Address']);
                } else {
                    $_SESSION['errors']->errorAdd($array['Address'].' No es un mail correcto : '.__FILE__.' ('.__LINE__.')');
                }
            }
        }

        if( isset($array['Attachment']) )
        {
            if( !is_array($array['Attachment']) )
            {
                $mail->addAttachment($array['Attachment']);
            } else {
                foreach( $array['Attachment'] AS $value )
                {
                    if( !is_array($value) )
                    {
                        $mail->addAttachment($value);
                    } else {
                        $_SESSION['errors']->errorAdd('No se espera array $value : '.__FILE__.' ('.__LINE__.')');
                        continue;
                    }
                }
            }
        }

        if( !isset($array['Subject']) )
        {
            $array['Subject'] = DEFAULT_SMTP_SUBJECT;
        } 
        $mail->Subject = $array['Subject'];

        if( isset($array['Body']) )
        {
            $mail->Body = $array['Body'];
        }

        if( isset($array['MsgHTML']) )
        {
            $mail->MsgHTML($body);
        }

        if(!$mail->Send())
        {
            $_SESSION['errors']->errorAdd('Error al mandar mail ('.$mail->ErrorInfo.') : '.__FILE__.' ('.__LINE__.')');
            return false;
        } else {
            return true;
        }
    }

    /**
	 * Obtiene la ip del visitante
	 *
	 * @method string ipGet() Obtiene la ip del visitante
	 * @access public
	 * @return string
	 */
    public function ipGet()
	{
        $_SESSION['errors']->otherAdd('LLamando al método $this->ipGet() : '.__FILE__.' ('.__LINE__.')');
		if (isset($_SERVER["HTTP_CLIENT_IP"]))
		{
			return $_SERVER["HTTP_CLIENT_IP"];
		} elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			return $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
			return $_SERVER["HTTP_X_FORWARDED"];
		} elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
			return $_SERVER["HTTP_FORWARDED_FOR"];
		} elseif (isset($_SERVER["HTTP_FORWARDED"])) {
			return $_SERVER["HTTP_FORWARDED"];
		} else {
			return $_SERVER["REMOTE_ADDR"];
		}
	}

    /**
     * Regresa el Modulo y el método de la url
     *
     * @method array urlParse() Regresa el Modulo y el método de la url
     * @access public
     * @param string $url
     * @return array
     */
    public function urlParse($url = FALSE)
    {
        if( !isset($url) OR $url == '' )
        {
            $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        }

        str_replace('http://www.', '', $url);
        str_replace('https://www.', '', $url);
        str_replace('http://', '', $url);
        str_replace('https://', '', $url);
        str_replace($_SERVER['HTTP_HOST'], '', $url);

        $urlArray = explode('/', $url);
        if( isset($urlArray[2]) )
        {
            return [$urlArray[1], $urlArray[2]];
        } elseif( isset($urlArray[1]) ) {
            return [$urlArray[1]];
        } else {
            return $url;
        }
    }

	/**
     * Crea carpetas en una estructura anidada
     *
     * @method folderCreate($route)
     * @access public
     * @param string $route
     * @return boolean
     */
	public function folderCreate($route = FALSE, $delete = FALSE)
	{
		if( !isset($route) OR $route == '' )
        {
            $route = DEFAULT_IMG_ENTRADAS;
        }

        if( !isset($delete) OR $delete == '' )
        {
            $delete = FALSE;
        }

		if ( !file_exists($route))
		{
			try
			{
				mkdir($route,0777,true);
			} catch(ErrorException $ex) {
				$_SESSION['errors']->errorAdd('No se pudo crear carpeta '.$ex->getMessage().' : '.__FILE__.' ('.__LINE__.')');
				return FALSE;
			}
		}

		if ( !is_dir($route))
		{
            if($delete)
            {
                unlink($route);
            } else {
                return FALSE;
            }

			try
			{
				mkdir($route,0777,true);
			} catch(ErrorException $ex) {
				$_SESSION['errors']->errorAdd('No se pudo crear carpeta '.$ex->getMessage().' : '.__FILE__.' ('.__LINE__.')');
				return FALSE;
			}
		}
		return $route;
	}

	/**
     * Crea subcarpetas año y mes en una estructura anidada
     *
     * @method subFolderCreate($route)
     * @access public
     * @param string $route
     * @return boolean
     */
	public function subFolderCreate($route = FALSE, $delete = FALSE)
	{
		if( !isset($route) OR $route == '' )
        {
            return FALSE;
        }

        if( !isset($delete) OR $delete == '' )
        {
            $delete = FALSE;
        }

		if (!file_exists($route))
		{
			try
			{
				mkdir($route,0777,true);
				chmod($route,0777);
			} catch(ErrorException $ex) {
				$_SESSION['errors']->errorAdd('No se pudo crear carpeta '.$ex->getMessage().' : '.__FILE__.' ('.__LINE__.')');
				return FALSE;
			}
		}

		if (!is_dir($route))
		{
            if($delete)
            {
                unlink(ROOT.$route);
            } else {
                return FALSE;
            }

			try
			{
				mkdir($route,0777,true);
				chmod($route,0777);
			} catch(ErrorException $ex) {
				$_SESSION['errors']->errorAdd('No se pudo crear carpeta '.$ex->getMessage().' : '.__FILE__.' ('.__LINE__.')');
				return FALSE;
			}
		}

		$date = new DateTime('NOW');
		$year = $date->format('Y');
		$month = $date->format('m');

		if(file_exists($route.DS.$year.DS.$month) AND is_dir($route.DS.$year.DS.$month))
		{
			return $route.DS.$year.DS.$month;	
		}

		if(file_exists($route.DS.$year.DS.$month) AND !is_dir($route.DS.$year.DS.$month) AND $delete == FALSE)
		{
			return false;
		}

		if(file_exists($route.DS.$year.DS.$month) AND !is_dir($route.DS.$year.DS.$month) AND $delete == TRUE)
		{
			unlink(ROOT.$route.DS.$year.DS.$month);
		}

		try
		{ 
			mkdir($route.DS.$year.DS.$month,0777,true);
			chmod($route,0777);
		} catch(ErrorException $ex) {
			$_SESSION['errors']->errorAdd('No se pudo crear carpeta '.$ex->getMessage().' : '.__FILE__.' ('.__LINE__.')');
			return FALSE;
		}
		return $route.DS.$year.DS.$month;
	}
	
	/**
     * Verifica que el archivo sea, en efecto, una imagen sin tomar en cunta su extension y no cualquier otro archivo que pueda dañar el servidor
     * Verifica que el tamaño de la imagen no exceda los 200kb
     * @method imageValidate($)
     * @access public
     * @param string $
     * @return boolean
     */
	public function imageValidate($imagePath = FALSE)
	{
		if (!is_array($imagePath))
		{			
			if(file_exists($imagePath))
			{
				$fileInfo = pathinfo($imagePath);
				$fileDim = getimagesize($imagePath);
				$imagePath = array("tmp_name" => $imagePath, "type" => $fileDim['mime'], "size" => filesize($imagePath));
			}
			else
			{
				return false;
			}
		}
	
		if ( !isset($imagePath) OR !is_array($imagePath) OR !isset($imagePath['tmp_name']) OR !isset($imagePath['type']) OR !isset($imagePath['size']) )
		{
			return false;
		}
	
		if ( !file_exists($imagePath['tmp_name']) )
		{
			return false;
		}
		
		$imageValidate = explode(',', DEFAULT_IMG_TYPE);
		$finfo = new finfo();
		$typeRutaTmp = $finfo->file($imagePath['tmp_name'], FILEINFO_MIME_TYPE);
		unset($finfo);
		
		if (!in_array($typeRutaTmp, $imageValidate))
		{
			return false;
		}
		
		$fileSource = getimagesize($imagePath['tmp_name']);

		if ($fileSource[0] < 150 OR $fileSource[1] < 150)
		{
			return false;
		}
		
		if ($imagePath['size'] > DEFAULT_IMG_SIZE)
		{
			return false;
		}
		return $typeRutaTmp;
	}

	public function imageUpload($upImg = FALSE, $folder = FALSE)
	{
		if (!isset($upImg) OR $upImg == '' )
		{
			$upImg = $_FILES;
		}

		if( !isset($folder) OR $folder == '')
		{		
			$folder = $this->subFolderCreate(DEFAULT_IMG_ENTRADAS);
			if(!$folder)
			{
				$folder = DEFAULT_IMG_ENTRADAS.DS.$year.DS.$month;
			}
		}
		
		$realExtension = $this->imageValidate($upImg['file']);
		
		if ($realExtension)
		{
			$fileInfo = pathinfo($upImg['file']['name']);
			$upImg['file']['name'] = $fileInfo['filename'].'.'.str_replace("image/", "", $realExtension);
			
			if(move_uploaded_file ($upImg['file']['tmp_name'], $folder.DS.$upImg['file']['name']))
			{
				$resize = $this->imageResize($upImg);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
	public function imageResize($upImg = FALSE, $folder = FALSE, $dimension = FALSE)
	{ 
		if( !isset($folder) OR $folder == '')
		{		
			$folder = $this->subFolderCreate(DEFAULT_IMG_ENTRADAS);
			if( !$folder )
			{
				$folder = DEFAULT_IMG_ENTRADAS.DS.$year.DS.$month;
			}
		}
		
		$defaultDim = explode(',', DEFAULT_IMG_PXSIZE);
		$dimension = '1080x768,885x650,1600x1200,800x250';
		if(!in_array($dimension, $defaultDim))
		{
			$dimension = explode(',',$dimension);
			
			foreach($dimension as $key => $value)
			{
				$dim = $value;
				$validString = "/^[[:digit:]]{3,4}(x{1})[[:digit:]]{3,4}$/i";
				if(preg_match($validString, $dim))
				{
					array_push($defaultDim, $dim);	
				}
			}
		}

		foreach($defaultDim as $key => $value) 
		{
			$dimBox = explode('x',$value);
			$fileSource = getimagesize($folder.DS.$upImg['file']['name']);
			$srcRatio = ($fileSource[0] /  $fileSource[1]);
			$boxRatio = ($dimBox[0] / $dimBox[1]);
			
			if($srcRatio > $boxRatio) {
				$width = ($dimBox[1] * $srcRatio);
				$height = $dimBox[1];
			} elseif($srcRatio < $boxRatio) {
				if($fileSource[0] > $fileSource[1]){
					$width = ($dimBox[1] * $srcRatio);
					$height = $dimBox[1];
				} else {
					$width = $dimBox[0];
					$height = ($dimBox[0] * $srcRatio);
				}
			} else {
				$width = $dimBox[0];
				$height = $dimBox[1];
			}
				
			$x = ($width - $dimBox[0]) / 2;
			$y = ($height - $dimBox[1]) / 2;
			
			$imageMimeType = $fileSource['mime'];
			$pathImg = $folder.DS.$upImg['file']['name'];
			switch ($imageMimeType)
			{
				case "image/jpeg":
					$image = imagecreatefromjpeg( $pathImg );
					break;
				case "image/png":
					$image = imagecreatefrompng( $pathImg );
					break;
				case "image/gif":
					$image = imagecreatefromgif( $pathImg );
					break;
			}
			
			$canvas = imagecreatetruecolor($dimBox[0], $dimBox[1]);		
			$tmpCanvas = imagecreatetruecolor($width, $height);
			
			imagecopyresampled( $tmpCanvas, $image, 0, 0, 0, 0, $width, $height, $fileSource[0], $fileSource[1] );
			imagecopy( $canvas, $tmpCanvas, 0, 0, $x, $y, $dimBox[0], $dimBox[1] );
			
			$fileInfo = pathinfo($upImg['file']['name']);
			
			switch ($imageMimeType)
			{
				case "image/jpeg":
					imagejpeg( $canvas, $folder."/".$fileInfo['filename']."-".$value."jpeg", 100 );
					break;
				case "image/png":
					imagepng( $canvas, $folder."/".$fileInfo['filename']."-".$value."png", 9 );
					break;
				case "image/gif":
					imagegif( $canvas, $folder."/".$fileInfo['filename']."-".$value."gif", 90 );
					break;
			}
		}
	}

    /**
     * Llama, guarda y valida parámetros pasados por $_POST o $_GET
     *
     * @method callPost()
     * @access public
     * @return array
     */
    public function callPost()
    {
        return $_POST;
    }
}
