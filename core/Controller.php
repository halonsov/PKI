<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase abstracta que va a ser extendida por todos los controladores
 */
abstract class Controller
{
	/**
     * Instancia a la base de datos
     *
     * @name $_model
     * @type PDO
     * @access protected
     */
	protected $_model;

	/**
     * Funciones generales
     *
     * @name $_functions
     * @type \Core\Functions
     * @access protected
     */
	protected $_functions;

	/**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
    public function __construct()
    {
		$_SESSION['errors']->otherAdd('Creando objeto \Core\Controller : '.__FILE__.' ('.__LINE__.')');
        $model = '\\'.$_SESSION['MODULE_ACTUAL'].'\\Models\\'.AutoLoad::modelLoad(DEFAULT_MODEL);
		$_SESSION['errors']->otherAdd('Creando objeto new '.$model.'() : '.__FILE__.' ('.__LINE__.')');
		$this->_model = new $model();
		$_SESSION['errors']->otherAdd('Creando objeto new \Core\Functions() : '.__FILE__.' ('.__LINE__.')');
		$this->_functions = new \Core\Functions();
    }

	/**
	 * Función por defecto
	 *
	 * @method string index() Función por defecto
	 * @access public
	 * @abstract method
	 */
	abstract public function index();

	/**
	 * Carga modelo dado
	 *
	 * @method string modelLoad() Carga modelo dado
	 * @access protected
	 * @param string $model
	 * @return string
	 */
	final protected function modelLoad($model = FALSE)
	{
		$_SESSION['errors']->otherAdd('LLamando al método $this->modelLoad('.$model.') : '.__FILE__.' ('.__LINE__.')');
		if( !isset($model) OR $model == '')
		{
			$_SESSION['errors']->warningAdd('No está definido $model o es nulo, cargando DEFAULT_MODEL : '.__FILE__.' ('.__LINE__.')');
			$controller = DEFAULT_CONTROLLER;
		}
		if(AutoLoad::modelLoad($model))
		{
			return $model;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.$model.' cargando DEFAULT_MODEL : '.__FILE__.' ('.__LINE__.')');
			$model = DEFAULT_MODEL;
			return AutoLoad::model($model);
		}
	}
}