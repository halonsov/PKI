<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;
use PDO;

/**
 * Clase abstracta para conectarse a la base de datos USA PDO
 */
abstract class DataBase
{
	/**
     * Instancia a la base de datos
     *
     * @name $_instance
     * @type \Core\DataBase
     * @access private
     */
    private static $_instance = null;

	/**
     * Host de la base de datos
     *
     * @name $_host
     * @type \Core\DataBase
     * @access private
     */
    private static $_host = DEFAULT_DB_HOST;

	/**
     * Usuario de la base de datos
     *
     * @name $_usr
     * @type \Core\DataBase
     * @access private
     */
    private static $_usr = DEFAULT_DB_USER;

	/**
     * Contraseña de la base de datos y el usuario
     *
     * @name $_pass
     * @type \Core\DataBase
     * @access private
     */
    private static $_pass = DEFAULT_DB_PASS;

	/**
     * Base de datos a la que se va a conectar
     *
     * @name $_database
     * @type \Core\DataBase
     * @access private
     */
    private static $_database = DEFAULT_DB_DB;

	/**
     * Codificación de caracteres de la base de datos
     *
     * @name $_char
     * @type \Core\DataBase
     * @access private
     */
    private static $_char = DEFAULT_DB_CHAR;

	/**
	 * Regresa instancia a la conexión de la base de datos
	 *
	 * @method string instanceGet() Regresa instancia a la conexión de la base de datos
	 * @access public
	 * @return PDO
	 */
    public static function instanceGet()
    {
		$_SESSION['errors']->otherAdd('LLamando al método $this->instanceGet() : '.__FILE__.' ('.__LINE__.')');
        if (self::$_instance)
		{
            return self::$_instance;
        } else {
			$_SESSION['errors']->otherAdd('Conectando a la base de datos : '.__FILE__.' ('.__LINE__.')');
            self::$_instance = new PDO('mysql:host=' . self::$_host . '; dbname='. self::$_database, self::$_usr, self::$_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".self::$_char));
            return self::$_instance;
        }
    }
}