<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @category Core
 */


/**
 * Controlador de controladores por defecto a cargar
 */
define("GENERAL_CONTROLLER", "main");

/**
 * Controlador por defecto a cargar
 */
define("DEFAULT_CONTROLLER", "index");

/**
 * Modelo por defecto a cargar
 */
define("DEFAULT_MODEL", "index");

/**
 * Módulo por defecto a cargar
 */
define("DEFAULT_MODULE", "home");

/**
 * Método del controlador de controladores por defecto a cargar
 */
define("DEFAULT_METHOD", "index");

/**
 * Título de la pestaña por defecto a cargar
 */
define("DEFAULT_TITLE", "PKI");

/**
 * Módulo a cargar en caso de no encontrar el módulo buscado (error 404)
 */
define("DEFAULT_404", "notFound");

/**
 * Url (http) del sitio
 */
define("BASE_URL", "http://{$_SERVER['HTTP_HOST']}/" );

/**
 * Url (https) del sitio
 */
define("SECURE_BASE_URL", "https://{$_SERVER['HTTP_HOST']}/");

/**
 * Carpeta en la que se encuentran los controladores
 */
define("CONTROLLER_DIR", "controllers");

/**
 * Carpeta en la que se encuentran las librerias
 */
define("LIB_DIR", ROOT . "lib" . DS);

/**
 * Carpeta en la que se encuentran los módulos
 */
define("MODULES_DIR", ROOT . "modules");

/**
 * Carpeta en la que se encuentran los modelos
 */
define("MODEL_DIR", "models");

/**
 * Carpeta en la que se encuentran las vistas
 */
define("VIEW_DIR", "views");

/**
 * Carpeta en la que se encuentras los archivos "multimedia"
 */
define("DEFAULT_MEDIA", DS."media".DS);

/**
 * Carpeta en la que se encuentran los css
 */
define("DEFAULT_CSS", DEFAULT_MEDIA.'css'.DS);

/**
 * Carpeta en la que se encuentras los archivos js
 */
define("DEFAULT_JS", DEFAULT_MEDIA.'js'.DS);

/**
 * Host de la base de datos
 */
define("DEFAULT_DB_HOST", "");

/**
 * Usuario de la base de datos
 */
define("DEFAULT_DB_USER", "");

/**
 * Contraseña del usuario de la base de datos
 */
define("DEFAULT_DB_PASS", "");

/**
 * Base de datos a la que se va a conectar
 */
define("DEFAULT_DB_DB", "");

/**
 * Codificación de los caracteres de la base de datos
 */
define("DEFAULT_DB_CHAR", "utf8");

/**
 * Puerto por defecto de smtp
 */
define("DEFAUL_SMTP_PORT" , 587);

/**
 * Seguridad por defecto de smtp
 */
define("DEFAULT_SMTP_SECURE", "tls");

/**
 * Host por defecto smtp
 */
define("DEFAULT_SMTP_HOST", "smtp.gmail.com");

/**
 * Usuario por defecto smtp
 */
define("DEFAULT_SMTP_USSER", "");

/**
 * Contraña del usuario por defecto smtp
 */
define("DEFAULT_SMTP_PASSWORD", "");

/**
 * Correo por defecto smtp
 */
define("DEFAULT_SMTP_FROM_MAIL", "");

/**
 * Nombre por defecto de smtp
 */
define("DEFAULT_SMTP_FROM_NAME", "");

/**
 * Asunto por defecto smtp
 */
define("DEFAULT_SMTP_SUBJECT", "Hola");

/**
 * Submenú activo por defecto
 */
define("DEFAULT_MENU", 1);

/**
 * Perfil por defecto para usuarios no logueados
 */
define("DEFAULT_PROFILE", 1);

/**
 * Imágen por defecto usuario
 */
define("DEFAULT_USSER_IMAGE", "/media/img/usuario.png");

/**
 * Scripts js a incluir
 */
define("DEFAULT_JS_SCRIPTS",
       '/media/js/jquery-1.11.0.min.js,'.
       '/media/js/jquery-ui-1.9.2.custom.js,'.
       '/media/js/functions.js,'.
       '/media/js/form-validator/jquery.form-validator.js,'.
       '/media/js/foundation/foundation.js,'.
       '/media/js/foundation/modernizr.js,'.
       '/media/js/foundation/foundation.offcanvas.js,'.
       '/media/js/foundation/foundation.joyride.js,'.
       '/media/js/foundation/foundation.tooltip.js,'.
       '/media/js/foundation/foundation.topbar.js,'.
       '/media/js/foundation/foundation.alert.js,'.
       '/media/js/foundation/foundation.accordion.js'
    );

/**
 * Directorio por default a guardar las imagenes de las entradas
 */
define("DEFAULT_IMG_ENTRADAS", ROOT.'/media/img/entradas');

/**
 * Dimensiones por defecto de las imagenes de las entradas
 */
define("DEFAULT_IMG_PXSIZE",
       '600x400,'.
       '150x100'
       );

/**
 * Tipo de archivo por defecto de las imagenes de las entradas
 */
define("DEFAULT_IMG_TYPE",
        'image/gif,'.
        'image/jpeg,'.
        'image/jpg,'.
        'image/pjpeg,'.
        'image/x-png,'.
        'image/png'
        );

/**
 *Tamaño de archivo por defecto de las imagenes de las entradas
 */
define("DEFAULT_IMG_SIZE", '200000');

/**
 * Archivo css por defecto
 */
define('DEFAULT_CSS_SCRIPTS',
       'foundation.css,'.
       'imports/notifications.css'
    );