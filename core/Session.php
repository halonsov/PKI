<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase para manejar las variables de sesión del cliente 
 */
class Session
{

	/**
	 * Inicia sesión
	 *
	 * @method void init() Inicia sesión
	 * @access public
	 * @return void
	 */
	final public function init()
	{
		session_start();
	}

	/**
	 * Obtiene la ip del usuario
	 *
	 * @method string ipGet() Obtiene la ip del usuario
	 * @access public
	 * @return string
	 */
	final public function ipGet()
	{
		if (isset($_SERVER["HTTP_CLIENT_IP"]))
		{
			return $_SERVER["HTTP_CLIENT_IP"];
		} elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			return $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
			return $_SERVER["HTTP_X_FORWARDED"];
		} elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
			return $_SERVER["HTTP_FORWARDED_FOR"];
		} elseif (isset($_SERVER["HTTP_FORWARDED"])) {
			return $_SERVER["HTTP_FORWARDED"];
		} else {
			return $_SERVER["REMOTE_ADDR"];
		}
	}

	/**
	 * Destruye la sesión del usuario
	 *
	 * @method void destroy() Destruye la sesión del usuario
	 * @access public
	 * @return void
	 */
	final public function destroy($keys = FALSE)
	{
		if($keys)
		{
			if(is_array($keys))
			{
				for($i=0;$i<count($keys);$i++)
				{
					if(isset($_SESSION[$keys[$i]]))
					{
						unset($_SESSION[$keys[$i]]);
					}
				}
			} else {
				if(isset($_SESSION[$keys]))
				{
					unset($_SESSION[$keys]);
				}
			}
		}else {
			session_destroy();
		}
	}

	/**
	 * Crea variable de sesión
	 *
	 * @method void set() Crea variable de sesión
	 * @acces public
	 * @param string $keys
	 * @param string $value
	 * @return void
	 */
	final public function set($keys, $value)
	{
		if(!empty($keys))
		{
			$_SESSION[$keys] = $value;
		}
	}

	/**
	 * Devuelve valor de variable de sesión
	 *
	 * @method string get() Devuelve valor de variable de sesión
	 * @access public
	 * @param string $keys
	 * @return string
	 */
	final public function get($keys)
	{
		if(isset($_SESSION[$keys]))
		{
			return $_SESSION[$keys];
		}
	}

	/**
	 * Valida la sesión del usuario
	 *
	 * @method boolean issAuth() Valida la sesión del usuario
	 * @access public
	 * @return boolean
	 */
	public function issAuth()
	{
		if(!isset($_SESSION['browserUSR']))
		{
			return false;
		} elseif($_SESSION['browserUSR'] != $_SERVER['HTTP_USER_AGENT']) {
			return false;
		} elseif(!isset($_SESSION['ipUSR'])){
			return false;
		} elseif($_SESSION['ipUSR'] != \Core\Functions::ipGet()){
			return false;
		} elseif( !isset($_SESSION['idUSR']) OR !isset($_SESSION['nameUSR']) OR !isset($_SESSION['usserUSR']) OR !isset($_SESSION['mailUSR']) OR !isset($_SESSION['profileUSR'])){
			return false;
		} else {
			return true;
		}
	}
}