<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase "debugeador" del sitio TO_DO discutir si es un debugueador o un log
 */
class Errors
{
    /**
     * Nivel de errores a mostrar. 0 ninguno, 1 errores, 2 avisos y errores, 3 todos (avisos, errores y otros TO_DO cambiar otros por info)
     *
     * @name $_level
     * @type \Core\Errors
     * @access private
     */
    private $_level;

    /**
     * Errores
     *
     * @name $_errors
     * @type \Core\Errors
     * @access private
     */
    private $_errors = [];

    /**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
    final public function __construct()
    {
        $this->_level = 0;
    }

    /**
	 * Establece el nivel de errores a mostrar
	 *
	 * @method void levetSet() Establece el nivel de errores a mostrar
	 * @access public
	 * @param int $level
	 * @return void
	 */
    final public function levetSet($level)
    {
        $_SESSION['errors']->otherAdd('Ejecutando levelSet('.$level.') : '.__FILE__.' ('.__LINE__.')');
        if($level != '0' AND $level != '1' AND $level != '2' AND $level != '3')
        {
            $_SESSION['errors']->errorAdd('Nivel de errores no valido ('.$level.'), estableciendo nivel de errores en 3 : '.__FILE__.' ('.__LINE__.')');
            $this->_level = 3;
        } else {
            $this->_level = $level;
        }
    }

    /**
	 * Regresa el nivel de errores
	 *
	 * @method void levelGet() Regresa el nivel de errores
	 * @access public
	 * @return void
	 */
    final public function levelGet()
    {
        $_SESSION['errors']->otherAdd('Ejecutando levelGet() : '.__FILE__.' ('.__LINE__.')');
        return $this->_level;
    }

    /**
	 * Agrega error del tipo otro
	 *
	 * @method void otherAdd() Agrega error del tipo otro
	 * @param string $message
	 * @access public
	 * @return void
	 */
    final public function otherAdd($message)
    {
        $this->_errors[] = ['class' => 'otro', 'message' => $message];
    }

    /**
	 * Agrega error del tipo aviso
	 *
	 * @method void warningAdd() Agrega error del tipo aviso
	 * @param string $message
	 * @access public
	 * @return void
	 */
    final public function warningAdd($message)
    {
        $this->_errors[] = ['class' => 'aviso', 'message' => $message];
    }

    /**
	 * Agrega error del tipo error
	 *
	 * @method void errorAdd() Agrega error del tipo error
	 * @param string $message
	 * @access public
	 * @return void
	 */
    final public function errorAdd($message)
    {
        $this->_errors[] = ['class' => 'error', 'message' => $message];
    }

    /**
     * Regresa los errores a mostrar
     *
     * @method string errorsGet() Regresa los errores a mostrar
     * @access public
     * @return string
     */
    final public function errorsGet()
    {
        $_SESSION['errors']->otherAdd('LLamando al método $this->errorsGet() : '.__FILE__.' ('.__LINE__.')');
        $errores = '';
        foreach($this->_errors  AS $value)
        {
            if($this->_level == 3)
            {
                $errores .= '<div class="small-12 large-12 columns '.$value['class'].'">'.$value['message'].'</div>';
            } else if($this->_level == 2) {
                if($value['class'] == 'error' OR $value['class'] == 'aviso')
                {
                    $errores .= '<div class="small-12 large-12 columns '.$value['class'].'">'.$value['message'].'</div>';
                }
            } else if($this->_level == 1) {
                if($value['class'] == 'error')
                {
                    $errores .= '<div class="small-12 large-12 columns '.$value['class'].'">'.$value['message'].'</div>';
                }
            }
        }

        return $errores;
    }
}