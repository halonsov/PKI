<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase que procesa la petición al sitio (url)
 */
class Request
{
	/**
     * Modulo a cargar
     *
     * @name $_module
     * @type string
     * @access private
     */
	private $_module;

	/**
     * Controlador a cargar
     *
     * @name $_controller
     * @type string
     * @access private
     */
	private $_controller;

	/**
     * Método del objeto a ejecutar
     *
     * @name $_method
     * @type string
     * @access private
     */
	private $_method;

	/**
     * Parámetros a pasarte al método
     *
     * @name $_arguments
     * @type array
     * @access private
     */
	private $_arguments;

	/**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
	final public function __construct()
	{
		$_SESSION['errors']->otherAdd('Construyendo objeto  \Core\Request() : '.__FILE__.' ('.__LINE__.')');
		if (isset($_GET["url"]))
		{
			$url = filter_input(INPUT_GET, "url", FILTER_SANITIZE_URL);
			$url = explode("/", $url);
			$url = array_filter($url);
			$this->_module = strtolower(array_shift($url));
			$this->_method = strtolower(array_shift($url));
			$this->_arguments = $url;
			unset($_GET);
		}
		if(!$this->_module)
		{
			$this->_module = DEFAULT_MODULE;
		}
		if(!$this->_controller)
		{
			$this->_controller = GENERAL_CONTROLLER;
		}
		if(!$this->_method)
		{
			$this->_method = DEFAULT_METHOD;
		}
		if(!isset($this->_arguments))
		{
			$this->_arguments = array();
		}
	}

	/**
	 * Obtiene el método a ejecutar
	 *
	 * @method string methodGet() Obtiene el método a ejecutar
	 * @access public
	 * @return string
	 */
	final public function methodGet()
	{
		$_SESSION['errors']->otherAdd('Llamando al método $this->methodGet() : '.__FILE__.' ('.__LINE__.')');
		return $this->_method;
	}

	/**
	 * Obtiene los argumentos a pasarle al método
	 *
	 * @method array argumentsGet() Obtiene los argumentos a pasarle al método
	 * @access public
	 * @return array
	 */
	final public function argumentsGet()
	{
		$_SESSION['errors']->otherAdd('Llamando al método $this->argumentsGet() : '.__FILE__.' ('.__LINE__.')');
		return $this->_arguments;
	}

	/**
	 * Obtiene el módulo
	 *
	 * @method array moduleGet() Obtiene el módulo
	 * @access public
	 * @return array
	 */
	final public function moduleGet()
	{
		$_SESSION['errors']->otherAdd('Llamando al método $this->moduleGet() : '.__FILE__.' ('.__LINE__.')');
		return $this->_module;
	}
}