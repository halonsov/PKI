<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase que carga todos los archivos (MMVC  (Module Model View Controller) y librerias (php) ) que requiera el sitio de manera dinámica
 */
class AutoLoad
{
	/**
     * Petición por URL
     *
     * @name $_petition
     * @type \Core\Request
     * @access private
     */
	private $_petition;

	/**
     * Modulo a cargar
     *
     * @name $_module
     * @type string
     * @access private
     */
	private $_module;

	/**
     * Controlador General
     *
     * @name $_generalController
     * @type string
     * @access private
     */
	private static $_generalController;

	/**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		$_SESSION['errors']->otherAdd('Construyendo objeto \Core\AutoLoad() : '.__FILE__.' ('.__LINE__.')');
		$_SESSION['errors']->otherAdd('Creando objeto  \Core\Request() : '.__FILE__.' ('.__LINE__.')');
		$this->_petition = new \Core\Request();
		$this->_generalController = GENERAL_CONTROLLER.'Module';
	}

	/**
	 * Carga el modulo dado
	 *
	 * @method boolean moduleLoad() Carga el modulo dado
	 * @access public
	 * @param string $module
	 * @return boolean
	 */
	final public function moduleLoad($module = FALSE)
    {
		if( !isset($module) OR $module == '')
		{
			$_SESSION['errors']->warningAdd('No está definido el modulo a cargar $this->moduleLoad() : '.__FILE__.' ('.__LINE__.')');
			$module = DEFAULT_MODULE;
		}
		$this->_module = $module.'Module';

		if($module != DEFAULT_404)
		{
			if( !isset( $_SESSION['permissions']['/'.$module]))
			{
				$_SESSION['errors']->errorAdd('No está cargado el módulo '.$module.' : '.__FILE__.' ('.__LINE__.')');
				return false;
			} elseif( !in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$module]['perfil'] )) {
				$_SESSION['errors']->warningAdd('No tienes los permissions para ver este módulo ('.$module.') : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
		$_SESSION['errors']->otherAdd('Llamando al método $this->moduleLoad('.$this->_module.') : '.__FILE__.' ('.__LINE__.')');
		if (file_exists(MODULES_DIR.DS.$this->_module.DS))
		{
			if (file_exists(MODULES_DIR.DS.$this->_module.DS.$this->_generalController.'.php'))
			{
				$_SESSION['errors']->otherAdd('LLamando al archivo '.MODULES_DIR.DS.$this->_module.DS.$this->_generalController.'.php : '.__FILE__.' ('.__LINE__.')');
				require_once MODULES_DIR.DS.$this->_module.DS.$this->_generalController.'.php';
				$_SESSION['MODULE_ACTUAL'] = $this->_module;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('No extiste el archivo '.MODULES_DIR.DS.$this->_module.DS.$this->_generalController.'.php : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
			return true;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.MODULES_DIR.DS.$this->_module.DS.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
    }

	/**
	 * Carga el controlador dado
	 *
	 * @method string controllerLoad() Carga el controlador dado
	 * @access public
	 * @param string $controller
	 * @return string
	 * @example $controlador = new controllerLoad('controladorAcargar');
	 */
	final public function controllerLoad($controller = FALSE)
    {
		if( !isset($controller) OR $controller == '')
		{
			$_SESSION['errors']->warningAdd('No está definido el controlador a cargar : '.__FILE__.' ('.__LINE__.')');
			$controller = DEFAULT_CONTROLLER.'Controller';
		} else {
			$controller = $controller.'Controller';
		}

		$_SESSION['errors']->otherAdd('Llamando al método $this->controllerLoad('.$controller.') : '.__FILE__.' ('.__LINE__.')');
		if (file_exists(MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.CONTROLLER_DIR.DS.$controller.'.php'))
		{
			$_SESSION['errors']->otherAdd('LLamando al archivo '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.CONTROLLER_DIR.DS.$controller.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			require_once MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.CONTROLLER_DIR.DS.$controller.'.php';
			return $controller;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.CONTROLLER_DIR.DS.$controller.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
    }

	/**
	 * Carga el modelo dado
	 *
	 * @method string modelLoad() Carga el modelo dado
	 * @access public
	 * @param string $model
	 * @return string
	 * @example $modelo = new modelLoad('modeloAcargar');
	 */
	final public function modelLoad($model = FALSE)
    {
		if( !isset($model) OR $model == '')
		{
			$_SESSION['errors']->warningAdd('No está definido el modelo a cargar : '.__FILE__.' ('.__LINE__.')');
			$model = DEFAULT_MODEL.'Model';
		} else {
			$model = $model.'Model';
		}

		$_SESSION['errors']->otherAdd('Llamando al método $this->modelLoad('.$model.') : '.__FILE__.' ('.__LINE__.')');
		if (file_exists(MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.MODEL_DIR.DS.$model.'.php'))
		{
			$_SESSION['errors']->otherAdd('LLamando al archivo '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.MODEL_DIR.DS.$model.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			require_once MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.MODEL_DIR.DS.$model.'.php';
			return $model;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.MODEL_DIR.DS.$model.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
    }

	/**
	 * Carga la vista dada
	 *
	 * @method boolean viewLoad() Carga la vista dada
	 * @access public
	 * @param string $view
	 * @return boolean
	 */
	final public function viewLoad($view = FALSE)
    {
		if( !isset($view) OR $view == '')
		{
			$_SESSION['errors']->otherAdd('No está definida la vista a cargar : '.__FILE__.' ('.__LINE__.')');
			$view = DEFAULT_MODEL.'View';
		} else {
			$view = $view.'View';
		}

		$_SESSION['errors']->otherAdd('Llamando al método $this->viewLoad('.$view.') : '.__FILE__.' ('.__LINE__.')');
		if (file_exists(MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.VIEW_DIR.DS.$view.'.php'))
		{
			$_SESSION['errors']->otherAdd('LLamando al archivo '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.VIEW_DIR.DS.$view.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			require_once MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.VIEW_DIR.DS.$view.'.php';
			return true;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.MODULES_DIR.DS.$_SESSION['MODULE_ACTUAL'].DS.VIEW_DIR.DS.$view.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
    }

	/**
	 * Carga la vista dada
	 *
	 * @method boolean libLoad() Carga la libreria dada
	 * @access public
	 * @param string $lib
	 * @param string $dir
	 * @return boolean
	 */
	final public function libLoad($lib = FALSE, $dir = FALSE)
    {
		if( !isset($lib) OR $lib == '')
		{
			$_SESSION['errors']->errorAdd('No está definida la libreria a cargar : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if(!isset($dir) OR $dir == '')
		{
			$_SESSION['errors']->warningAdd('No está definido el directorio : '.__FILE__.' ('.__LINE__.')');
			$dir = '';
		} else {
			$dir = $dir.DS;
		}

		$_SESSION['errors']->otherAdd('Llamando al método $this->libLoad('.$lib.', '.$dir.') : '.__FILE__.' ('.__LINE__.')');
		if (file_exists(LIB_DIR.DS.$dir.DS.$lib.'.php'))
		{
			$_SESSION['errors']->otherAdd('LLamando al archivo  '.LIB_DIR.DS.$dir.DS.$lib.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			require_once LIB_DIR.DS.$dir.DS.$lib.'.php';
			return true;
		} else {
			$_SESSION['errors']->errorAdd('No existe '.LIB_DIR.DS.$dir.DS.$lib.'.php'.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
    }

	/**
	 * Carga el modulo dado y ejecuta el metodo dado con los parametros dados
	 *
	 * @method boolean run() Carga la libreria dada
	 * @access public
	 * @return boolean
	 */
	final public function run()
	{
		$_SESSION['errors']->otherAdd('Ejecutando el método $this->run() : '.__FILE__.' ('.__LINE__.')');
		$newModule = $this->_petition->moduleGet();
		if($this->moduleLoad($newModule))
		{
			$_SESSION['errors']->otherAdd('Llamado a $this->Module("'.$newModule.'") correcto : '.__FILE__.' ('.__LINE__.')');
			$mod = '\\'.$_SESSION['MODULE_ACTUAL'].'\\'.$this->_generalController;
			$_SESSION['errors']->otherAdd('Creando objeto '.$mod.'($this->_petition) : '.__FILE__.' ('.__LINE__.')');
			$module = new $mod($this->_petition);
			$method = $this->_petition->methodGet();
			if( !is_callable(array($module, $method)) )
			{
				$_SESSION['errors']->warningAdd('No existe el método '.$mod.'::'.$method.'() , llamando a DEFAULT_METHOD : '.__FILE__.' ('.__LINE__.')');
				$method = DEFAULT_METHOD;
			} elseif( !isset($_SESSION['permissions']['/'.$newModule]['metodo']['/'.$method]) ) {
				$_SESSION['errors']->warningAdd('No está habilitado el método '.$mod.'::'.$method.'() , llamando a DEFAULT_METHOD : '.__FILE__.' ('.__LINE__.')');
				$method = DEFAULT_METHOD;
			}
			if( !in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$newModule]['metodo']['/'.$method]['perfil']) )
			{
				$_SESSION['errors']->warningAdd('No tienes permissions para el método '.$mod.'::'.$method.'() , llamando a DEFAULT_METHOD : '.__FILE__.' ('.__LINE__.')');
				$newModule = DEFAULT_404;
				$this->moduleLoad(DEFAULT_404);
				$mod = '\\'.$_SESSION['MODULE_ACTUAL'].'\\'.$this->_generalController;
				$_SESSION['errors']->otherAdd('Creando objeto '.$mod.'($this->_petition) : '.__FILE__.' ('.__LINE__.')');
				$module = new $mod($this->_petition);
				$method = $this->_petition->methodGet();
				if( !is_callable(array($module, $method)) )
				{
					$_SESSION['errors']->warningAdd('No existe el método '.$mod.'::'.$method.'() , llamando a DEFAULT_METHOD : '.__FILE__.' ('.__LINE__.')');
					$method = DEFAULT_METHOD;
				}
				$arg = $this->_petition->argumentsGet();
				if(isset($arg))
				{
					call_user_func_array(array($module, $method), $arg);
					$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'($arg) : '.__FILE__.' ('.__LINE__.')');
				} else {
					$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'() : '.__FILE__.' ('.__LINE__.')');
					$module->$method();
				}
				return false;
			}
			$_SESSION['errors']->otherAdd('LLamando al método $this->_petition->argumentsGet() : '.__FILE__.' ('.__LINE__.')');
			$arg = $this->_petition->argumentsGet();
			if(isset($arg))
			{
				call_user_func_array(array($module, $method), $arg);
				$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'($arg) : '.__FILE__.' ('.__LINE__.')');
			} else {
				$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'() : '.__FILE__.' ('.__LINE__.')');
				$module->$method();
			}
			return true;
		} else {
			$_SESSION['errors']->errorAdd('Error al llamar $this->Module('.$newModule.'), llamando a $this->moduleLoad(DEFAULT_404) : '.__FILE__.' ('.__LINE__.')');
			$newModule = DEFAULT_404;
			$this->moduleLoad(DEFAULT_404);
			$mod = '\\'.$_SESSION['MODULE_ACTUAL'].'\\'.$this->_generalController;
			$_SESSION['errors']->otherAdd('Creando objeto '.$mod.'($this->_petition) : '.__FILE__.' ('.__LINE__.')');
			$module = new $mod($this->_petition);
			$method = $this->_petition->methodGet();
			if( !is_callable(array($module, $method)) )
			{
				$_SESSION['errors']->warningAdd('No existe el método '.$mod.'::'.$method.'() , llamando a DEFAULT_METHOD : '.__FILE__.' ('.__LINE__.')');
				$method = DEFAULT_METHOD;
			}
			$arg = $this->_petition->argumentsGet();
			if(isset($arg))
			{
				call_user_func_array(array($module, $method), $arg);
				$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'($arg) : '.__FILE__.' ('.__LINE__.')');
			} else {
				$_SESSION['errors']->otherAdd('LLamando al método '.$mod.'::'.$method.'() : '.__FILE__.' ('.__LINE__.')');
				$module->$method();
			}
			return false;
		}
	}

	/**
	 * Regresa la petición por URL
	 *
	 * @method void petitionGet() Regresa la petición por URL
	 * @access public
	 * @return \Core\Request
	 */
	final public function petitionGet()
	{
		$_SESSION['errors']->otherAdd('Llamando al método $this->petitionGet() : '.__FILE__.' ('.__LINE__.')');
		return $this->_petition;
	}
}