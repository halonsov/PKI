<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 */
class Model
{
	/**
     * Query que nos trae el menú del sitio
     *
     * @name $menuGet
     * @type SQL
     * @access private
     */
	private static $menuGet = <<<SQL
	SELECT
		name_men, parent_men, order_men, link_men

	FROM
		menus

	ORDER BY 
		parent_men ASC, 
		order_men ASC
SQL;

	/**
     * Query que nos trae los permissions de los módulos
     *
     * @name $modulesPermissionsGet
     * @type SQL
     * @access private
     */
	private static $modulesPermissionsGet = <<<SQL
	SELECT
		MO.name_mod, MO.url_mod, MXP.profile_mxp, ME.name_met, ME.url_met, ME.active_met, MXE.profile_mtp

	FROM
		modules MO

	INNER JOIN
		modulesXprofiles MXP

		ON
			MXP.module_mxp = MO.id_mod

	INNER JOIN
		methods ME

		ON
			ME.module_met = MO.id_mod

	INNER JOIN
		methodsXprofiles MXE

		ON
			MXE.method_mtp = ME.id_met

	WHERE
		MO.active_mod = 1 AND ME.active_met = 1
SQL;

	/**
     * Instancia a la base de datos
     *
     * @name $_db
     * @type \Core\Model
     * @access protected
     */
	protected $_db;

	/**
     * Funciones generales
     *
     * @name $_functions
     * @type \Core\Functions
     * @access protected
     */
	protected $_functions;

	/**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		$_SESSION['errors']->otherAdd('Creando objeto \Core\Model : '.__FILE__.' ('.__LINE__.')');
		$this->_db = \Core\DataBase::instanceGet();
		$this->_functions = new \Core\Functions();
	}

	/**
	 * Entrecomilla una cadena de caracteres para usarla en una consulta
	 *
	 * @method string quoting()  Entrecomilla una cadena de caracteres para usarla en una consulta
	 * @access public
	 * @param string $string
	 * @return string
	 */
	final protected function quoting($string)
	{
		$_SESSION['errors']->otherAdd('LLamando al método $this->quoting($string) : '.__FILE__.' ('.__LINE__.')');
		return $this->_db->quote($string);
	}

	/**
	 * Obtiene el menú del sitio
	 *
	 * @method array menuGet() Obtiene el menú del sitio
	 * @access public
	 * @return array
	 */
	final public function menuGet()
	{
		$_SESSION['errors']->otherAdd('LLamando al método $this->menuGet() : '.__FILE__.' ('.__LINE__.')');
		$stmt = $this->_db->prepare(self::$menuGet);
        $stmt->execute();
		$menu = [];
		foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $key => $value)
		{
			$urlParse = $this->_functions->urlParse($value['link_men']);
			if($value['parent_men'] == 0)
			{
				if(count($urlParse) == 1 AND is_array($urlParse))
				{
					if( isset($_SESSION['permissions']['/'.$urlParse[0]]) )
					{
						if( in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$urlParse[0]]['perfil']) )
						{
							$menu[$value['order_men']]['menu'] = $value['name_men'];
							$menu[$value['order_men']]['link'] = $value['link_men'];
						} else {
							continue;
						}
					} else {
						$menu[$value['order_men']]['menu'] = $value['name_men'];
						$menu[$value['order_men']]['link'] = $value['link_men'];
					}
				} elseif(count($urlParse) == 2 AND is_array($urlParse)) {
					if( isset($_SESSION['permissions']['/'.$urlParse[0]]['metodo']['/'.$urlParse[1]]) )
					{
						if( in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$urlParse[0]]['metodo']['/'.$urlParse[1]]['perfil']) )
						{
							$menu[$value['order_men']]['menu'] = $value['name_men'];
							$menu[$value['order_men']]['link'] = $value['link_men'];
						} else {
							continue;
						}
					} else {
						$menu[$value['order_men']]['menu'] = $value['name_men'];
						$menu[$value['order_men']]['link'] = $value['link_men'];
					}
				} else {
					$menu[$value['order_men']]['menu'] = $value['name_men'];
					$menu[$value['order_men']]['link'] = $value['link_men'];
				}
				
			} else {
				if(count($urlParse) == 1 AND is_array($urlParse))
				{
					if( isset($_SESSION['permissions']['/'.$urlParse[0]]) )
					{
						if( in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$urlParse[0]]['perfil']) )
						{
							$menu[$value['parent_men']]['submenu'][$value['order_men']]['menu'] = $value['name_men'];
							$menu[$value['parent_men']]['submenu'][$value['order_men']]['link'] = $value['link_men'];
						} else {
							continue;
						}
					} else {
						$menu[$value['parent_men']]['submenu'][$value['order_men']]['menu'] = $value['name_men'];
						$menu[$value['parent_men']]['submenu'][$value['order_men']]['link'] = $value['link_men'];
					}
					continue;
				} elseif(count($urlParse) == 2 AND is_array($urlParse)) {
					if( isset($_SESSION['permissions']['/'.$urlParse[0]]['metodo']['/'.$urlParse[1]]) )
					{
						if( in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.$urlParse[0]]['metodo']['/'.$urlParse[1]]['perfil']) )
						{
							$menu[$value['parent_men']]['submenu'][$value['order_men']]['menu'] = $value['name_men'];
							$menu[$value['parent_men']]['submenu'][$value['order_men']]['link'] = $value['link_men'];
						} else {
							continue;
						}
					} else {
						$menu[$value['parent_men']]['submenu'][$value['order_men']]['menu'] = $value['name_men'];
						$menu[$value['parent_men']]['submenu'][$value['order_men']]['link'] = $value['link_men'];
					}
				} else {
					$menu[$value['parent_men']]['submenu'][$value['order_men']]['menu'] = $value['name_men'];
					$menu[$value['parent_men']]['submenu'][$value['order_men']]['link'] = $value['link_men'];
				}
			}
		}
		return $menu;
	}

	/**
	 * Obtiene los modulos con permissions
	 *
	 * @method array modulesPermissionsGet() Obtiene los modulos con permissions
	 * @access public
	 * @return array
	 */
	final public function modulesPermissionsGet()
	{
		$_SESSION['errors']->otherAdd('LLamando al método $this->modulesPermissionsGet() : '.__FILE__.' ('.__LINE__.')');
		$stmt = $this->_db->prepare(self::$modulesPermissionsGet);
        $stmt->execute();
		$mod = [];
		foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $key => $value)
		{
			$mod[$value['url_mod']]['nombre'] = $value['name_mod'];
			if( !isset($mod[$value['url_mod']]['perfil']) )
			{
				$mod[$value['url_mod']]['perfil'] = [];
			}
			if( !in_array($value['profile_mxp'], $mod[$value['url_mod']]['perfil']) )
			{
				$mod[$value['url_mod']]['perfil'][] = $value['profile_mxp'];
			}
			$mod[$value['url_mod']]['metodo'][$value['url_met']]['nombre'] = $value['name_met'];
			if( !isset($mod[$value['url_mod']]['metodo'][$value['url_met']]['perfil']) )
			{
				$mod[$value['url_mod']]['metodo'][$value['url_met']]['perfil'] = [];
			}
			if( !in_array($value['profile_mtp'], $mod[$value['url_mod']]['metodo'][$value['url_met']]['perfil']) )
			{
				$mod[$value['url_mod']]['metodo'][$value['url_met']]['perfil'][] = $value['profile_mtp'];
			}
		}
		return $mod;
	}
}