<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase que procesa todas las llamadas a las vistas
 */
class View
{
	/**
	 * Array que contiene los css a incluir
	 * 
	 * @name $_css
	 * @access private
	 * @var array
	 * @example array('archivo1.css','archivo2.css','archivo3.css')
	 */
    private $_css = [];

	/**
	 * Array que contiene los scripts js a incluir
	 * 
	 * @name $_js
	 * @access private
	 * @var array
	 * @example array('archivo1.js','archivo2.js','archivo3.js')
	 */
    private $_js = [];

	/**
	 * Array que contiene scripts definidos por el "usuario"
	 * 
	 * @name $_scriptsEmbedded
	 * @access private
	 * @var var
	 * @example array( 'js' => array( '$(document).ready(function(){alert("Hola");})' ) , 'css' => array( '@prueba { color: #FFFFFF;}' ) )
	 */
    private $_scriptsEmbedded = [
		'js'	=>	[],
		'css'	=>	[]
	];

	/**
	 * Array que contiene los scripts css ya incluidos en el header
	 * 
	 * @name $_cssDefault
	 * @access private
	 * @var array
	 * @example array('archivo1.css','archivo2.css','archivo3.css')
	 */
	private $_cssDefault;

	/**
	 * Array que contiene los scripts js ya incluidos en el header
	 * 
	 * @name $_jsDefault
	 * @access private
	 * @var array
	 * @example array('archivo1.js','archivo2.js','archivo3.js')
	 */
	private $_jsDefault;

	/**
     * Mensajes a mostar (/header.php)
     *
     * @name $_messages
     * @access private
	 * @var array
     */
	private $_messages = [];

	/**
     * Constructor por defecto
     *
     * @method void __construct() Constructor por defecto
     * @access public
     * @return void
     */
    public function __construct()
	{
		$this->_cssDefault = explode(',', DEFAULT_CSS_SCRIPTS);
		$this->_jsDefault = explode(',', DEFAULT_JS_SCRIPTS);
		$_SESSION['errors']->otherAdd('Construyendo objeto  \Core\View(Request $petition) : '.__FILE__.' ('.__LINE__.')');
    }

	/**
	 * Carga la vista del modulo actual
	 *
	 * @method void render() Carga la vista del modulo actual
	 * @access public
	 * @param string $view
	 * @return void
	 */
    public function render($view)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->render($view, $item=FALSE, $ajax=FALSE, $layout = "default") : '.__FILE__.' ('.__LINE__.')');
		require_once ROOT."header.php";
		$viewPath = \Core\AutoLoad::viewLoad($view);
		if ( !$viewPath )
		{
            $_SESSION['errors']->errorAdd('Error al Mostrar la página : '.__FILE__.' ('.__LINE__.')');
        }
		require_once ROOT."footer.php";
    }

	/**
	 * Regresa todos los scripts a cargar
	 *
	 * @method string parameterGet() Regresa todos los scripts a cargar
	 * @access public
	 * @return string
	 */
    public function parameterGet()
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
		$return = '';
        if(count($this->_css) > 0 )
		{
			$_SESSION['errors']->otherAdd('Hay archivos css a incluir $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
			foreach($this->_css AS $value)
			{
				$return .= '<link rel="stylesheet" href="'.DEFAULT_CSS.$value.'" type="text/css" />';
			}
		} else {
			$_SESSION['errors']->otherAdd('No hay archivos css a incluir $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
		}

		if( count($this->_js) > 0)
		{
			$_SESSION['errors']->otherAdd('Hay archivos js a incluir $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
			foreach($this->_js AS $value)
			{
				$return .= '<script src="'.DEFAULT_JS.$value.'" type="text/javascript" ></script>';
			}
		} else {
			$_SESSION['errors']->otherAdd('No hay archivos js a incluir $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
		}

		if(is_array($this->_scriptsEmbedded['js']) AND count($this->_scriptsEmbedded['js']) > 0)
		{
			$_SESSION['errors']->otherAdd('Hay scripts js definidos $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
			$js = '';
			foreach($this->_scriptsEmbedded['js'] AS $value)
			{
				$js .= $value;
			}
			$return .= '<script type="text/javascript" >'.$js.'</script>';
		} else {
			$_SESSION['errors']->otherAdd('No hay scripts js definidos $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
		}

		if(is_array($this->_scriptsEmbedded['css']) AND count($this->_scriptsEmbedded['css']) > 0)
		{
			$_SESSION['errors']->otherAdd('Hay scripts css definidos $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
			$css = '';
			foreach($this->_scriptsEmbedded['css'] AS $value)
			{
				$css .= $value;
			}
			$return .= '<style type="text/css">'.$css.'</style>';
		} else {
			$_SESSION['errors']->otherAdd('No hay scripts css definidos $this->parameterGet() : '.__FILE__.' ('.__LINE__.')');
		}

        return $return;
    }

	/**
	 * Agrega un nuevo archivo css
	 *
	 * @method boolean cssSet() Agrega un nuevo archivo css
	 * @access public
	 * @param string/array $css
	 * @return boolean
	 */
	public function cssSet($css)
	{
		if( is_array($css) AND count($css) > 0)
		{
			$_SESSION['errors']->otherAdd('Llamando al metétodo $this->cssSet(array()) : '.__FILE__.' ('.__LINE__.')');
			$return = true;
			foreach($css AS $value)
			{
				if( !file_exists(ROOT.DEFAULT_CSS.$value) )
				{
					$_SESSION['errors']->errorAdd('No existe el archivo '.DEFAULT_CSS.$value.' : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else if( substr($value,-4) != '.css') {
					$_SESSION['errors']->errorAdd('El archivo '.$value.' no tiene extención css  : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else if( in_array($value, $this->_cssDefault)){
					$_SESSION['errors']->errorAdd('El archivo '.DEFAULT_CSS.$value.' ya está incluido  : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else {
					$_SESSION['errors']->otherAdd('Existe el archivo '.DEFAULT_CSS.$value.' : '.__FILE__.' ('.__LINE__.')');
					$this->_css[] = $value;
				}
			}
			return $return;
		} else {
			$_SESSION['errors']->otherAdd('Llamando al metétodo $this->cssSet('.$css.') : '.__FILE__.' ('.__LINE__.')');
			if( !file_exists(ROOT.DEFAULT_CSS.$css) )
			{
				$_SESSION['errors']->errorAdd('No existe el archivo '.DEFAULT_CSS.$css.' : '.__FILE__.' ('.__LINE__.')');
				return false;
			} else if( substr($css,-4) != '.css') {
				$_SESSION['errors']->errorAdd('El archivo '.$css.' no tiene extención css  : '.__FILE__.' ('.__LINE__.')');
				return false;
			} else if( in_array($css, $this->_cssDefault)){
				$_SESSION['errors']->errorAdd('El archivo '.DEFAULT_CSS.$css.' ya está incluido  : '.__FILE__.' ('.__LINE__.')');
				$return = false;
			} else {
				$_SESSION['errors']->otherAdd('Existe el archivo '.DEFAULT_CSS.$css.' : '.__FILE__.' ('.__LINE__.')');
				$this->_css[] = $css;
				return true;
			}
		}
	}

	/**
	 * Agrega un nuevo archivo js
	 *
	 * @method boolean jsSet() Agrega un nuevo archivo js
	 * @access public
	 * @param string/array $js
	 * @return boolean
	 */
	public function jsSet($js)
	{
		if( is_array($js) AND count($js) > 0)
		{
			$_SESSION['errors']->otherAdd('Llamando al metétodo $this->jsSet(array()) : '.__FILE__.' ('.__LINE__.')');
			$return = true;
			foreach($js AS $value)
			{
				if( !file_exists(ROOT.DEFAULT_JS.$value) )
				{
					$_SESSION['errors']->errorAdd('No existe el archivo '.DEFAULT_JS.$value.' : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else if( substr($value,-4) != '.js') {
					$_SESSION['errors']->errorAdd('El archivo '.$value.' no tiene extención js  : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else if( in_array($value, $this->_jsDefault)){
					$_SESSION['errors']->errorAdd('El archivo '.DEFAULT_JS.$value.' ya está incluido  : '.__FILE__.' ('.__LINE__.')');
					$return = false;
				} else {
					$_SESSION['errors']->otherAdd('Existe el archivo '.DEFAULT_JS.$value.' : '.__FILE__.' ('.__LINE__.')');
					$this->_js[] = $value;
				}
			}
			return $return;
		} else {
			$_SESSION['errors']->otherAdd('Llamando al metétodo $this->jsSet('.$js.') : '.__FILE__.' ('.__LINE__.')');
			if( !file_exists(ROOT.DEFAULT_JS.$js) )
			{
				$_SESSION['errors']->errorAdd('No existe el archivo '.DEFAULT_JS.$js.' : '.__FILE__.' ('.__LINE__.')');
				return false;
			} else if( substr($js,-3) != '.js') {
				$_SESSION['errors']->errorAdd('El archivo '.$js.' no tiene extención js  : '.__FILE__.' ('.__LINE__.')');
				return false;
			} else if( in_array($js, $this->_jsDefault)){
				$_SESSION['errors']->errorAdd('El archivo '.DEFAULT_JS.$js.' ya está incluido  : '.__FILE__.' ('.__LINE__.')');
				$return = false;
			} else {
				$_SESSION['errors']->otherAdd('Existe el archivo '.DEFAULT_JS.$js.' : '.__FILE__.' ('.__LINE__.')');
				$this->_js[] = $js;
				return true;
			}
		}
	}

	/**
	 * Agrega un nuevo script definido por el usuario
	 *
	 * @method boolean scriptsEmbeddedSet() Agrega un nuevo script definido por el usuario
	 * @access public
	 * @param array $scripts
	 * @return void
	 */
	public function scriptsEmbeddedSet($scripts)
	{
		if( is_array($scripts) AND count($scripts) > 0)
		{
			$_SESSION['errors']->otherAdd('Llamando scriptsEmbeddedSet($scripts) : '.__FILE__.' ('.__LINE__.')');
			if(!isset($scripts['js']))
			{
				$_SESSION['errors']->otherAdd('$scripts["js"] no está definido : '.__FILE__.' ('.__LINE__.')');
			} else if(!is_array($scripts['js'])) {
				$_SESSION['errors']->errorAdd('$scripts["js"] no es array : '.__FILE__.' ('.__LINE__.')');
			} else if( count($scripts['js']) == 0) {
				$_SESSION['errors']->errorAdd('$scripts["js"] es array vacio : '.__FILE__.' ('.__LINE__.')');
			} else {
				foreach($scripts['js'] AS $value)
				{
					$this->_scriptsEmbedded['js'][] = $value;
				}
			}

			if(!isset($scripts['css']))
			{
				$_SESSION['errors']->otherAdd('$scripts["css"] no está definido : '.__FILE__.' ('.__LINE__.')');
			} else if(!is_array($scripts['css'])) {
				$_SESSION['errors']->errorAdd('$scripts["css"] no es array : '.__FILE__.' ('.__LINE__.')');
			} else if( count($scripts['css']) == 0) {
				$_SESSION['errors']->errorAdd('$scripts["css"] es array vacio : '.__FILE__.' ('.__LINE__.')');
			} else {
				foreach($scripts['css'] AS $value)
				{
					$this->_scriptsEmbedded['css'][] = $value;
				}
			}
		} else {
			$_SESSION['errors']->errorAdd('Error al llamar el método scriptsEmbeddedSet($scripts), $scripts no es array : '.__FILE__.' ('.__LINE__.')');
		}
	}

	/**
	 * Agrega un nuevo mensaje a mostrar
	 *
	 * @method boolean messageAdd() Agrega un nuevo mensaje a mostrar
	 * @access public
	 * @param array $message
	 * @return boolean
	 */
	public function messageAdd($message)
	{
		if( is_array($message))
		{
			if( !isset($message['type']) OR !isset($message['message']) OR $message['message'] == '')
			{
				return false;
			} elseif( is_array($message['type'])) {
				return false;
			}

			if($message['type'] == '')
			{
				$message['type'] = 'info';
			}

			if( is_array($message['message']) ){
				foreach($message['message'] AS $value)
				{
					if( is_array($value) OR $value == '')
					{
						continue;
					} else {
						if($message['type'] != 'info' AND $message['type'] != 'warning' AND $message['type'] != 'success' )
						{
							$message['type'] = 'info';
						}
						$this->_messages[$message['type']][] = $value;
						continue;
					}
				}
				return true;
			}else {
				if($message['type'] != 'info' AND $message['type'] != 'warning' AND $message['type'] != 'success' )
				{
					$message['type'] = 'info';
				}
				$this->_messages[$message['type']][] = $message['message'];
			}
		} else {
			$this->_messages['info'][] = $message;
			return true;
		}
	}

	/**
	 * Devuelve los mensajes a mostrar al usuario
	 *
	 * @method string messageGet() Devuelve los mensajes a mostrar al usuario
	 * @access public
	 * @return string
	 */
	public function messageGet()
	{
		if(count($this->_messages) > 0)
		{
			$message = '';
			foreach($this->_messages AS $key => $value)
			{
				foreach($value AS $value2)
				{
					$message .= '<div data-alert class="alert-box '.$key.' round">'.$value2.'<a href="#" class="close">&times;</a></div>';
				}
			}
			return $message;
		} else {
			return '';
		}
	}
}