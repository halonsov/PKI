<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace Core;

/**
 * Clase abstracta que va a ser extendida por todos los modelos
 */
abstract class Module
{

	/**
     * Instancia a la vista
     *
     * @name $_views
     * @type \Core\Views
     * @access protected
     */
	protected $_view;

	/**
     * Funciones generales
     *
     * @name $_functions
     * @type \Core\Functions
     * @access protected
     */
	protected $_functions;

	/**
	 * Constructor por defecto
	 *
	 * @method void __construct() Constructor por defecto
	 * @access public
	 * @return void
	 */
    public function __construct(\Core\Request $request)
    {
		$_SESSION['errors']->otherAdd('Creando objeto \Core\Module() : '.__FILE__.' ('.__LINE__.')');
		$_SESSION['errors']->otherAdd('Creando objeto \Core\View() : '.__FILE__.' ('.__LINE__.')');
        $this->_view = new \Core\View();
		$_SESSION['errors']->otherAdd('LLamando al método \Core\AutoLoad::modelLoad(DEFAULT_MODEL) : '.__FILE__.' ('.__LINE__.')');
		\Core\AutoLoad::modelLoad(DEFAULT_MODEL);
		$_SESSION['errors']->otherAdd('Creando objeto \Core\Functions() : '.__FILE__.' ('.__LINE__.')');
		$this->_functions = new \Core\Functions();
		$_SESSION['errors']->otherAdd('Creando objeto \Core\Model() : '.__FILE__.' ('.__LINE__.')');
		$model = new \Core\Model();
		$_SESSION['errors']->otherAdd('LLamando al método \Core\Model::menuGet() : '.__FILE__.' ('.__LINE__.')');
		$this->_view->menu = $model->menuGet();
    }

	abstract public function index();

	/**
	 * Carga controlador dado
	 *
	 * @method \MODULE_ACTUAL\Controllers\controller controllerLoad() Carga controlador dado
	 * @access protected
	 * @param string $controller
	 * @return \MODULE_ACTUAL\Controllers\controller
	 */
	final protected function controllerLoad($controller = FALSE)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->controllerLoad() : '.__FILE__.' ('.__LINE__.')');
		if( !isset($controller) OR $controller == '')
		{
			$_SESSION['errors']->warningAdd('No está definido $controller o es nulo, cargando DEFAULT_CONTROLLER : '.__FILE__.' ('.__LINE__.')');
			$controller = DEFAULT_CONTROLLER;
		}
		$_SESSION['errors']->otherAdd('Llamando al metétodo \Core\AutoLoad::controllerLoad('.$controller.') : '.__FILE__.' ('.__LINE__.')');
		$controller = \Core\AutoLoad::controllerLoad($controller);
		if($controller)
		{
			$contr = '\\'.$_SESSION['MODULE_ACTUAL'].'\\Controllers\\'.$controller;
			$_SESSION['errors']->otherAdd('Creando objeto \\'.$_SESSION['MODULE_ACTUAL'].'\\Controllers\\'.$controller.'() : '.__FILE__.' ('.__LINE__.')');
			return new $contr();
		} else {
			$_SESSION['errors']->errorAdd('No existe '.$controller.', cargando DEFAULT_METHOD  : '.__FILE__.' ('.__LINE__.')');
			$controller = \Core\AutoLoad::controllerLoad(DEFAULT_CONTROLLER);
			$contr = '\\'.$_SESSION['MODULE_ACTUAL'].'\\Controllers\\'.$controller;
			$_SESSION['errors']->otherAdd('Creando objeto \\'.$_SESSION['MODULE_ACTUAL'].'\\Controllers\\'.$controller.'() : '.__FILE__.' ('.__LINE__.')');
			return new $contr();
		}
	}

	/**
	 * Carga modulo dado
	 *
	 * @method \MODULE_ACTUAL\GENERAL_CONTROLLER controllerLoad() Carga modulo dado
	 * @access protected
	 * @param string $module
	 * @return \MODULE_ACTUAL\GENERAL_CONTROLLER
	 */
	final protected function moduleLoad($module = FALSE)
	{
		if( !isset($module) OR $module == '')
		{
			$_SESSION['errors']->errorAdd('No está definido $module, o es nulo, tomando DEFAULT_MODULE : '.__FILE__.' ('.__LINE__.')');
			$module = DEFAULT_MODULE;
		}
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->moduleLoad('.$module.') : '.__FILE__.' ('.__LINE__.')');
		$return = new \Core\AutoLoad();
		$_SESSION['errors']->otherAdd('Llamando al metétodo \Core\AutoLoad::moduleLoad('.$module.') : '.__FILE__.' ('.__LINE__.')');
		if( $return->moduleLoad($module) )
		{
			$module = '\\'.$_SESSION['MODULE_ACTUAL'].'\\'.GENERAL_CONTROLLER.'Module';
			$_SESSION['errors']->otherAdd('Creando objeto \\'.$_SESSION['MODULE_ACTUAL'].'\\'.GENERAL_CONTROLLER.'Module() : '.__FILE__.' ('.__LINE__.')');
			return new $module(new \Core\Request());
		} else {
			$_SESSION['errors']->errorAdd('No existe módulo '.$module.' cargando el módulo DEFAULT_404 : '.__FILE__.' ('.__LINE__.')');
			$return->moduleLoad(DEFAULT_404);
			$module = '\\'.$_SESSION['MODULE_ACTUAL'].'\\'.GENERAL_CONTROLLER.'Module';
			$_SESSION['errors']->otherAdd('Creando objeto \\'.$_SESSION['MODULE_ACTUAL'].'\\'.GENERAL_CONTROLLER.'Module() : '.__FILE__.' ('.__LINE__.')');
			return new $module(new \Core\Request());
		}
	}

	/**
	 * Devuelve un string convirtiendo caracteres especiales en HTML a entidades HTML
	 *
	 * @method string controllerLoad() Devuelve un string convirtiendo caracteres especiales en HTML a entidades HTML
	 * @access protected
	 * @param string $text
	 * @return string
	 */
	protected function textFilter($text)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->textFilter('.$text.') : '.__FILE__.' ('.__LINE__.')');
		if(isset($text) && !empty($text))
		{
			$text = htmlspecialchars($text, ENT_QUOTES);
			return $text;
		}
		return "";
	}

	/**
	 * Checa si se tienen permissions para usar el método
	 *
	 * @method boolean methodPermissions() Checa si se tienen permissions para usar el método
	 * @access protected
	 * @param string $method
	 * @return void
	 */
	protected function methodPermissions($method = FALSE)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->methodPermissions() : '.__FILE__.' ('.__LINE__.')');
		if( !isset($method) OR $method == '')
		{
			$_SESSION['errors']->warningAdd($method.' no está definido, o es nulo $this->methodPermissions($method), llamando a DEFAULT_404 : '.__FILE__.' ('.__LINE__.')');
			$module = $this->moduleLoad(DEFAULT_404);
			$module->index();
			exit;
		}

		if( !in_array($_SESSION['profileUSR'], $_SESSION['permissions']['/'.str_replace('Module', '',$_SESSION['MODULE_ACTUAL'])]['metodo']['/'.$method]['perfil']) )
		{
			$_SESSION['errors']->warningAdd('No se tienen permissions para usar el método '.$_SESSION['MODULE_ACTUAL'].'::'.$method.'() , llamando a DEFAULT_404 : '.__FILE__.' ('.__LINE__.')');
			$module = $this->moduleLoad(DEFAULT_404);
			$module->index();
			exit;
		}

		return true;
	}

	/**
	 * Carga el módulo por default
	 *
	 * @method void moduleDefaultLoad() Carga el módulo por default
	 * @access protected
	 * @param array $message
	 * return void
	 */
	protected function moduleDefaultLoad($message = FALSE)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->moduleDefaultLoad() : '.__FILE__.' ('.__LINE__.')');
		$module = $this->moduleLoad(DEFAULT_MODULE);
		if( isset($message) AND is_array($message))
		{
			if( isset($message['type']) AND isset($message['message']) )
			{
				$module->_view->messageAdd(['type' => $message['type'], 'message' => $message['message']]);
			}
		}
        $module->index();
		exit;
	}

	/**
	 * Carga el módulo 404
	 *
	 * @method void module404Load() Carga el módulo 404
	 * @access protected
	 * @param array $message
	 * @return void
	 */
	protected function module404Load($message = FALSE)
	{
		$_SESSION['errors']->otherAdd('Llamando al metétodo $this->module404Load() : '.__FILE__.' ('.__LINE__.')');
		$module = $this->moduleLoad(DEFAULT_404);
		if( isset($message) AND is_array($message))
		{
			if( isset($message['type']) AND isset($message['message']) )
			{
				$module->_view->messageAdd(['type' => $message['type'], 'message' => $message['message']]);
			}
		}
        $module->index();
		exit;
	}

	/**
	 * Establece el titulo de la página
	 *
	 * @method boolean pageTitleSet() Establece el titulo de la página
	 * @param string $method
	 * @param string titulo
	 * @return boolean
	 */
	public function pageTitleSet($title = FALSE)
	{
		if( isset($title) AND $title != '' )
		{
			$this->_view->pageTitle = $title;
			return true;
		}

		if( !isset($_SESSION['permissions']['/'.str_replace('Module', '', $_SESSION['MODULE_ACTUAL'])]['metodo']['/'.$method]['nombre']) OR $_SESSION['permissions']['/'.str_replace('Module', '', $_SESSION['MODULE_ACTUAL'])]['metodo']['/'.$method]['nombre'] == '')
		{
			$this->_view->pageTitle = DEFAULT_TITLE;
			return true;
		}

		$this->_view->pageTitle = $_SESSION['permissions']['/'.str_replace('Module', '', $_SESSION['MODULE_ACTUAL'])]['metodo']['/'.$method]['nombre'];
		return true;
	}
}