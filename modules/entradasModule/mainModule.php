<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @category Core
 */

namespace entradasModule;

class mainModule extends \Core\Module
{
    public function __construct(\Core\Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $this->module404Load();
        exit;
    }
    
    /**
	 * Carga el formulario de registro
	 *
	 * @method boolean () Carga el formulario para subir las noticias
	 * @access public
	 * @return boolean
	 */
    public function entrada()
    {
        $this->methodPermissions('entrada');
        $registra = $this->controllerLoad('registra');
        $this->pageTitleSet('registro');
        $this->_view->render("entradas");
        exit;
    }
    
    /**
     * Registra entrada 
	 *
	 * @method boolean () Registra la noticia
	 * @access public
	 * @return boolean
	 */
    public function entradas()
    {
        $this->methodPermissions('entradas');
        echo "hola";
    }
}
