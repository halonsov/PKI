<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @category Core
 */

namespace entradasModule\Models;

class indexModel extends \Core\Model
{
	private static $setEntrada = <<<SQL
	INSERT INTO
		entradas (
			titulo_ent, contenido_ent, fecPublicacion_ent, imagen_ent, resumen_ent, activo_ent, id_usr
		)
		VALUES (
			:titulo, :contenido, NOW(), :imagen, :resumen, :active, :usser
		)
SQL;

	private static $activaEntrada = <<<SQL
    UPDATE
        entradas

    SET
        activo_ent = 1

    WHERE
        id_ent = :entrada
SQL;

	private static $getEntrada = <<<SQL
	SELECT
		id_ent, titulo_ent, contenido_ent, fecPublicacion_ent, imagen_ent, resumen_ent, activo_ent, id_usr
	
	FROM
		entradas
		
	WHERE
		id_ent = :entrada
SQL;

    public function __construct()
    {
		parent::__construct();
    }
	
	public function setEntrada($entrada)
    {
        $stmt = $this->_db->prepare(self::$setEntrada);
        $stmt->bindValue(':titulo', $entrada['entrada']);
        $stmt->bindValue(':contenido', $entrada['contenido']);
        $stmt->bindValue(':imagen', $entrada['imagen']);
        $stmt->bindValue(':resumen', $entrada['resumen']);
		$stmt->bindValue(':active', $entrada['activo']);
        $stmt->bindValue(':usser', $entrada['usuario']);
        $stmt->execute();
        return $this->_db->lastInsertId();
    }
	
	public function activaEntrada($entrada)
    {
        $stmt = $this->_db->prepare(self::$activaEntrada);
        $stmt->bindValue(':entrada', $entrada);
        return $stmt->execute();
    }
	
	public function getUsuario($entrada)
	{
        $stmt = $this->_db->prepare(self::$getEntrada);
        $stmt->bindValue(':entrada', $entrada);
        $stmt->execute();
		$en = $this->_functions->fetchAllUnset($stmt->fetchAll());
		if( !isset($en[0]) )
		{
			return '';
		}
		$en = $en[0];
        $ent = [];
		$ent['titulo'] = $en['titulo_ent'];
		$ent['contenido'] = $en['contenido_ent'];
		$ent['fecRegistro'] = $en['fecRegistro_ent'];
		$ent['imagen'] = $en['imagen_ent'];
		$ent['resumen'] = $en['resumen_ent'];
		$ent['activo'] = $en['activo_ent'];
		$ent['usuario'] = $en['id_usr'];
		return $ent;
	}
}