<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace entradasModule\Controllers;

class entradasController extends \Core\Controller
{
	public function __construct()
    {
		parent::__construct();
    }

	public function index() {}
    
    public function registraEntrada($post = false)
    {
        if( !is_array($post) )
		{	
			$_SESSION['errors']->errorAdd('Se esperaba un array $post : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
        
        if( !isset($post['titulo']) OR $post['titulo'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'titulo\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$title = $post['titulo'];
		if( !$this->_functions->expressionValidate('alpha', $title) )
		{
			$_SESSION['errors']->errorAdd($title.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		
		if( !isset($post['contenido']) OR $post['contenido'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'contenido\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$title = $post['contenido'];
		if( !$this->_functions->expressionValidate('alpha', $contenido) )
		{
			$_SESSION['errors']->errorAdd($contenido.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		
		if( !isset($post['imagen']) OR $post['imagen'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'imagen\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$title = $post['imagen'];
		if( !$this->_functions->expressionValidate('alpha', $imagen) )
		{
			$_SESSION['errors']->errorAdd($imagen.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		
		if( !isset($post['resumen']) OR $post['resumen'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'resumen\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$title = $post['resumen'];
		if( !$this->_functions->expressionValidate('alpha', $resumen) )
		{
			$_SESSION['errors']->errorAdd($resumen.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		
		if( !isset($post['usuario']) OR $post['usuario'] == '' )
        {
            $_SESSION['errors']->errorAdd('No está definido $post[\'usuario\'] : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $usser = $post['usuario'];
		if( !$this->_functions->expressionValidate('usuario', $usser) )
		{
			$_SESSION['errors']->errorAdd($usser.' No es un usuario valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		
		$last_id = $this->_model->setEntrada([
				'titulo' 		=>	$title,
				'contenido'		=>	$contenido,
				'imagen'		=>	$imagen,
				'resumen'		=>	$resumen,
				'usuario'		=>	$usser,
				'activo'		=>	1
			]);
    }
}