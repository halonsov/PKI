<div class="small-12  large-12 large-centered columns">
    <form id="formulario_usuario" method="POST" name="formulario_usuario" action="<?= SECURE_BASE_URL."usuarios/registra";?>">
		<div class="row">
            <div class="small-12 large-5 large-centered columns">
                <h1>Añade una entrada</h1>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Título:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="titulo" name="titulo" type="text" placeholder="[A-Za-z]" maxlength="50" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" style="width: 800px;">
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Contenido:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="titulo" name="titulo" type="text" placeholder="[A-Za-z]" maxlength="50" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" style="height: 600px; width: 800px;">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>