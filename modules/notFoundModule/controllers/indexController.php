<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace notFoundModule\Controllers;

class indexController extends \Core\Controller
{
	private $_found;

	/**
	 * Construe el controlador, carga el modelo de pagos
	 * y el CSS general
	 */

	public function __construct()
	{
		parent::__construct();
	}

    public function index()
	{
		$this->_view->pageTitle = "404";
		$this->_view->error = $this->error->mensajeGet();
		$this->_view->render("index");
	}
}