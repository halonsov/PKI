<style>
    .error404 {
        background: #4d4174;
        padding: 50px;
        padding-bottom: 0px;
        text-align: center;
        color: #FFFFFF;
        font-size: 3.5rem;
        font-family: arial;
    }
    .faro {
        padding-top: 80px;
        padding-bottom: 80px;
    }
    .right {
        margin-bottom: 0px;
    }
    .ladrillo {
        background-image: url('/media/img/ladrillo.png');
        height: 50px;
        padding: 0px;
    }
</style>
<div class="small-12 large-12 columns error404">
    <div class="small-12 large-12 columns">
        <div class="small-3 large-3 columns faro">
            <img src='/media/img/faro.png'>
        </div>
        <div class="small-3 large-3 columns faro">
            <img src='/media/img/faro.png'>
        </div>
        <div class="small-3 large-3 columns faro">
            <img src='/media/img/faro.png'>
        </div>
        <div class="small-3 large-3 columns faro">
            <img src='/media/img/faro.png'>
        </div>
    </div>
    <div class="small-12 large-12 columns" style='padding-left: 0px; padding-right: 0px;'>
        <div class="small-8 large-8 columns" style='padding-left: 0px; padding-right: 0px;'>
            <img id='cake'src="/media/img/cake.png" style="width: 70px; border-top-width: 1px;" title='The cake is a lie!'>
        </div>
        <ul class='right' style='padding-left: 0px; padding-right: 0px;'>
            <div class="small-8 large-8 columns" style='padding-left: 0px; padding-right: 0px;'>
                <img id='champinion' src="/media/img/champinion.png" style="width: 70px; border-top-width: 1px;" title='Our page is in another castle!'>
            </div>
            <div class="small-4 large-4 columns" style='padding-left: 0px; padding-right: 0px;'>
                <img src='/media/img/puerta.png'>
            </div>
        </ul>
    </div>
</div>
<ol class="joyride-list" data-joyride="">
    <li data-id="champinion" data-options="tip_location:top">
        <h4>Thank you usser!</h4>
        <p>But our page is in another castle!</p>
    </li>
    <li data-id="cake" data-options="tip_location:top">
        <p>The cake is a lie!</p>
    </li>
    <li data-button="End">
        <p>But Still Alive!</p>
    </li>
</ol>
<div class="small-12 large-12 columns ladrillo"></div>