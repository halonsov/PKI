<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace ajaxModule\Controllers;

class indexController extends \Core\Controller
{
	public function __construct()
    {
		parent::__construct();
    }

	public function index() {}

	/**
	 * Trae los sexos en forma de option (HTML)
	 *
	 * @method string sexsOptionGet()
	 * @access public
	 * @return string
	 */
    public function sexsOptionGet()
    {
        return $this->_functions->arrayOptionSet($this->_model->sexGet());
    }

	/**
	 * Trae los países en forma de option
	 *
	 * @method string countriesOptionGet()
	 * @access public
	 * @return string
	 */
    public function countriesOptionGet()
    {
        return $this->_functions->arrayOptionSet($this->_model->countriesGet());
    }

	/**
	 * Trae las ciudades en forma de option
	 *
	 * @method string citiesOptionGet()
	 * @access public
	 * @return string
	 */
	public function citiesOptionGet($country)
	{
		return $this->_functions->arrayOptionSet($this->_model->citiesGet($country));
	}

	/**
	 * Trae las regiones en forma de option
	 *
	 * @method string regionsOptionGet()
	 * @access public
	 * @return string
	 */
	public function regionsOptionGet($city)
	{
		return $this->_functions->arrayOptionSet($this->_model->regionsGet($city));
	}
}