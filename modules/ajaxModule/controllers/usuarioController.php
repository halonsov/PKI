<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace ajaxModule\Controllers;

class usuarioController extends \Core\Controller
{
    public function __construct()
    {
		parent::__construct();
    }

    public function index() {}

	/**
	 * Verifica existencia en la base de datos de un nombre de usuario dado
	 *
	 * @method string usserNameExist()
	 * @access public
	 * @param string $usser
	 * @return string
	 */
    public function usserNameExist($usser)
	{
		$_SESSION['errors']->otherAdd('Creando objeto \modules\ajaxModule : '.__FILE__.' ('.__LINE__.')');
		return $this->_model->usserNameExist($usser);
	}

	/**
	 * Verifica existencia el na base de datos de un correo dado
	 *
	 * @method string usserMailExist()
	 * @access public
	 * @param string $mail
	 * @return boolean
	 */
	public function usserMailExist($mail)
	{
		$exist = $this->_model->usserMailExist($mail);
		return $exist;
	}

	/**
	 * Valida la contraseña de un usuario
	 *
	 * @method string usserValidatePass()
	 * @access public
	 * @param string $password
	 * @return boolean
	 */
    public function usserValidatePass($password)
    {
        return $this->_model->usserPassExist(\Core\Session::get('idUSR'), $password);
    }
}