<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace ajaxModule\Models;

/**
 * Modelo para cargar página de inicio
 */
class indexModel extends \Core\Model
{
	/**
	 * Trae sexos
	 *
	 * @name $sexGet
	 * @type SQL
	 * @access private
	 */
    private static $sexGet = <<<SQL
    SELECT
        id_sex, sex_sex

    FROM
        sexs
SQL;

	/**
	 * Trae países
	 *
	 * @name $countriesGet
	 * @type SQL
	 * @access private
	 */
    private static $countriesGet = <<<SQL
    SELECT
        id_cou, name_cou

    FROM
        countries
SQL;

	/**
	 * Trae ciudades
	 *
	 * @name $citiesGet
	 * @type SQL
	 * @access private
	 */
    private static $citiesGet = <<<SQL
    SELECT
        id_cit, nombre_cit

    FROM
        cities

    WHERE
        idPais_cit = :country
SQL;

	/**
	 * Trae regiones
	 *
	 * @name $regionGet
	 * @type SQL
	 * @access private
	 */
    private static $regionGet = <<<SQL
    SELECT
        id_reg, name_reg

    FROM
        regions

    WHERE
        idCity_reg = :city
SQL;

	/**
	 * Valida existencia de usuario
	 *
	 * @name $usserNameExist
	 * @type SQL
	 * @access private
	 */
    private static $usserNameExist = <<<SQL
    SELECT
        count(*) AS exist

    FROM
        ussers

    WHERE
        usser_usr = :usser
SQL;

	/**
	 * Valida existencia del correo
	 *
	 * @name $usserMailExist
	 * @type SQL
	 * @access private
	 */
    private static $usserMailExist = <<<SQL
    SELECT
        count(*) AS exist

    FROM
        ussers

    WHERE
        mail_usr = :mail
SQL;

	/**
	 * Valida la contraseña
	 *
	 * @name $usserPassValidate
	 * @type SQL
	 * @access private
	 */
	private static $usserPassValidate = <<<SQL
    SELECT
        count(*) AS exist

    FROM
        ussers

    WHERE
        id_usr = :usser AND password_usr = :password
SQL;

    public function __construct()
    {
		parent::__construct();
    }

	/**
	 * Trae sexos
	 *
	 * @method array sexGet()
	 * @access prublic
	 * @return array
	 */
    public function sexGet()
    {
        $stmt = $this->_db->prepare(self::$sexGet);
        $stmt->execute();
        $sex = [];
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $sex[$value['id_sex']] = $value['sex_sex'];
        }
        return $sex;
    }

	/**
	 * Trae países
	 *
	 * @method array countriesGet()
	 * @access prublic
	 * @return array
	 */
    public function countriesGet()
    {
        $stmt = $this->_db->prepare(self::$countriesGet);
        $stmt->execute();
        $countries = [];
        $i=0;
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $countries[$value['id_cou']] = $value['name_cou'];
        }
        return $countries;
    }

	/**
	 * Trae ciudades
	 *
	 * @method array citiesGet()
	 * @access prublic
	 * @param string $country
	 * @return array
	 */
    public function citiesGet($country)
    {
        $stmt = $this->_db->prepare(self::$citiesGet);
        $stmt->bindValue(':country', $country);
        $stmt->execute();
        $cities = [];
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $cities[$value['id_cit']] = $value['nombre_cit'];
        }
        return $cities;
    }

	/**
	 * Trae regiones
	 *
	 * @method array regionsGet()
	 * @access prublic
	 * @param string $city
	 * @return array
	 */
    public function regionsGet($city)
    {
        $stmt = $this->_db->prepare(self::$regionGet);
        $stmt->bindValue(':city', $city);
        $stmt->execute();
        $cities = [];
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $cities[$value['id_reg']] = $value['name_reg'];
        }
        return $cities;
    }

	/**
	 * Valida existencia de usuario
	 *
	 * @method boolean regionsGet()
	 * @access prublic
	 * @param string $usser
	 * @return boolean
	 */
    public function usserNameExist($usser)
    {
        $stmt = $this->_db->prepare(self::$usserNameExist);
        $stmt->bindValue(':usser', $usser);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
		return $exist['exist'];
    }

	/**
	 * Valida existencia de correo
	 *
	 * @method boolean usserMailExist()
	 * @access prublic
	 * @param string $usser
	 * @return boolean
	 */
    public function usserMailExist($mail)
    {
        $stmt = $this->_db->prepare(self::$usserMailExist);
        $stmt->bindValue(':mail', $mail);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
		return $exist['exist'];
    }

	/**
	 * Valida contraseña
	 *
	 * @method boolean usserPassExist()
	 * @access prublic
	 * @param string $usser
	 * @param string $password
	 * @return boolean
	 */
	public function usserPassExist($usser, $password)
	{
		
		$stmt = $this->_db->prepare(self::$usserPassValidate);
        $stmt->bindValue(':usser', $usser);
		$stmt->bindValue(':password', md5($password));
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
		return $exist['exist'];
	}
}