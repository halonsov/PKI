<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace ajaxModule;

class mainModule extends \Core\Module
{
    public function __construct(\Core\Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $this->module404Load();
        exit;
    }

    /**
     * Trae en forma de option información del mundo
     *
     * @method string worldGet()
     * @access public
     * @param string $get
     * @param string $date
     * @return string
     */
    public function worldget($get = FALSE, $date = FALSE)
    {
        //$this->methodPermissions('worldGet');
        $world = $this->controllerLoad('index');
        if( !isset($get) OR !isset($date) OR $get == '' OR $date == '' AND $get != 'pais')
        {
            echo '<option value=""default>---</option>';
            exit;
        } elseif($get == 'pais') {
            echo '<option value=""default>---</option>'.$world->countriesOptionGet();
            exit;
        } elseif($get == 'ciudad') {
            echo '<option value=""default>---</option>'.$world->citiesOptionGet($date);
            exit;
        } elseif($get == 'region') {
            echo '<option value=""default>---</option>'.$world->regionsOptionGet($date);
            exit;
        } else {
            echo '<option value=""default>---</option>';
            exit;
        }
    }

    /**
     * Valida existencia de usuario
     *
     * @method boolean usserExist()
     * @access public
     * @param string $field
     * @param string $usser
     * @return boolean
     */
    public function usserExist($field = FALSE, $usser = FALSE)
    {
        //$this->methodPermissions('usserExist');
        $exist = $this->controllerLoad('usuario');
        if( !isset($field) OR !isset($usser) OR $field == '' OR $usser == '')
        {
            echo 0;
            exit;
        } elseif($field == 'usuario') {
            echo $exist->usserNameExist($usser);
            exit;
        } elseif($field == 'correo') {
            echo $exist->usserMailExist($usser);
            exit;
        } else {
            echo 2;
            exit;
        }
    }

    /**
     * Valida usuario
     *
     * @method boolean usserValidate()
     * @access public
     * @param string $field
     * @param string $usser
     * @return boolean
     */
    public function usserValidate($field = FALSE, $usser = FALSE)
    {
        //$this->methodPermissions('usserValidate');
        $validate = $this->controllerLoad('usuario');
        if(!\Core\Session::get('isAuthUSR'))
        {
            echo 0;
            exit;
        }

        if( !isset($field) OR $field == '' OR !isset($usser) OR $usser == '')
        {
            echo 0;
            exit;
        }
        if($field == 'contrasenia')
        {
            echo $validate->usserValidatePass($usser);
            exit;
        } else {
            echo 0;
            exit;
        }
    }
}