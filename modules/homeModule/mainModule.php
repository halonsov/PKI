<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace homeModule;

class mainModule extends \Core\Module
{
    public function __construct(\Core\Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        /*$this->_functions->folderCreate();
		$this->_functions->imageUpload($_FILES);
        $dimension = '1080x980';
        /*if($this->_functions->imageValidate('/var/www/excited-puppy.sh'))
        {
            echo "A huevo!";
        } else {
            echo "nel";
        }*/
        $this->pageTitleSet('home');
        $this->_view->render("index");
    }
}