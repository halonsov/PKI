<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace homeModule\Controllers;

class indexController extends \Core\Controller
{
	public function __construct()
    {
		parent::__construct();
    }

	public function index() {}

	/**
	 * Pasrea información del mundo
	 *
	 * @method void worldParse()
	 * @access private
	 * @return void
	 */
	private function worldParse()
    {
        $file = fopen("6m", "r") or exit("Unable to open file!");
        while(!feof($file))
        {
			$row = fgets($file);
            $regionTroll = explode(',', str_replace("'", '', str_replace('"', '', $row) ));
			if($regionTroll[4] == "-")
			{
				$regionTroll[4] = $regionTroll[3];
			}
			if($regionTroll[5] == "-")
			{
				$regionTroll[5] = $regionTroll[3];
			}
			$region[$regionTroll[2]][$regionTroll[4]][$regionTroll[5]] = [
				'countryName'	=>	$regionTroll[3],
				'ipLower'		=>	$regionTroll[0],
				'ipHigher'		=>	$regionTroll[1],
				'latitude'		=>	$regionTroll[6],
				'longitude'		=>	$regionTroll[7],
				'zip'	=>	$regionTroll[8],
				'timeZone'		=>	$regionTroll[9],
				
			];
        }
        fclose($file);
		foreach($region AS $key1 => $value1)
		{
			foreach($value1 AS $key2 => $value2)
			{
				foreach($value2 AS $key3 => $value3)
				{
					if($this->_model->countryExist($key1) == 0)
					{
						$this->_model->countrySet($key1, $value3['countryName']);
					}

					if($this->_model->cityExist($key1, $key2) == 0)
					{
						$idCity = $this->_model->citySet($key1, $key2);
					} else {
						$idCity = $this->_model->idCityGet($key1, $key2);
					}

					if($this->_model->regionExist($idCity, $key3) == 0)
					{
						$idRegion = $this->_model->regionSet($idCity, $key3, $value3);
					} else {
						$idRegion = $this->_model->idRegionGet($idCity, $key3);
						$idRegion = $idRegion['id_reg'];
					}

					if($this->_model->ipXregionExist($idRegion, $value3['ipLower'], $value3['ipHigher']) == 0)
					{
						$ipXregion = $this->_model->ipXregionSet($idRegion , $value3['ipLower'], $value3['ipHigher']);
					}
				}
			}
		}
    }
}