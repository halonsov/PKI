<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace homeModule\Models;

/**
 * Modelo para cargar página de inicio
 */
class indexModel extends \Core\Model
{

    /**
     * Valida existencia de país
     *
     * @name $countryExist
     * @type SQL
     * @access private 
     */
    private static $countryExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        countries

    WHERE
        id_cou = :country
SQL;

    /**
     * Inserta país
     *
     * @name $co
     * @type SQL
     * @access private 
     */
    private static $countrySet = <<<SQL
    INSERT INTO
        countries(id_cou, name_cou)

    VALUES
        (:idCountry, :countryName)
SQL;

    /**
     * Verifica existencia de ciudad
     *
     * @name $cityExist
     * @type SQL
     * @access private 
     */
    private static $cityExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        cities

    WHERE
        idPais_cit = :country AND nombre_cit = :city
SQL;

    /**
     * Inserta ciudad
     *
     * @name $citySet
     * @type SQL
     * @access private 
     */
    private static $citySet = <<<SQL
    INSERT INTO
        cities(idPais_cit, nombre_cit)

    VALUES
        (:country, :city)
SQL;

    /**
     * Trae id de ciudad
     *
     * @name $idCityGet
     * @type SQL
     * @access private 
     */
    private static $idCityGet = <<<SQL
    SELECT
        id_cit

    FROM
        cities

    WHERE
        idPais_cit = :country AND nombre_cit = :city
SQL;

    /**
     * Verifica existencia de región
     *
     * @name $regionExist
     * @type SQL
     * @access private 
     */
    private static $regionExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        regions

    WHERE
        idCity_reg = :city AND name_reg = :region
SQL;

    /**
     * Trae id de región
     *
     * @name $idRegionGet
     * @type SQL
     * @access private 
     */
    private static $idRegionGet = <<<SQL
    SELECT
        id_reg

    FROM
        regions

    WHERE
        idCity_reg = :city AND name_reg = :region
SQL;

    /**
     * Inserta región
     *
     * @name $regionSet
     * @type SQL
     * @access private 
     */
    private static $regionSet = <<<SQL
    INSERT INTO
        regions(idCity_reg, name_reg, zipCode_reg, timeZone_reg, latitude_reg, longitude_reg)

    VALUES
        (:city, :region, :cp, :timeZone, :latitude, :longitude)
SQL;

    /**
     * Verifica existencia de ipXregion
     *
     * @name $ipXregionExist
     * @type SQL
     * @access private 
     */
    private static $ipXregionExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        ipXregions

    WHERE
        idRegion_ixr = :region AND ipLower_ixr = :ipLower AND ipHigher_ixr = :ipHigher
SQL;

    /**
     * Inserta ipXregion
     *
     * @name $ipXregionSet
     * @type SQL
     * @access private 
     */
    private static $ipXregionSet = <<<SQL
    INSERT INTO
        ipXregions(idRegion_ixr, ipLower_ixr, ipHigher_ixr)

    VALUES
        (:region, :ipLower, :ipHigher)
SQL;

    public function __construct()
    {
		parent::__construct();
    }

    /**
     * Verifica existencia de país
     *
     * @method boolean counrtyExist()
     * @access public
     * @param string $country
     * @return boolean
     */
    public function countryExist($country){
        $stmt = $this->_db->prepare(self::$countryExist);
        $stmt->bindValue(':country', $country);
        $stmt->execute();
        $countryExist = $this->_functions->fetchUnset($stmt->fetch());
        return $countryExist['exist'];
    }

    /**
     * Inserta país
     *
     * @method boolean countrySet()
     * @access public
     * @param string $idCountry
     * @param string $countryName
     * @return boolean
     */
    public function countrySet($idCountry, $countryName)
    {
        $stmt = $this->_db->prepare(self::$countrySet);
        $stmt->bindValue(':idCountry', $idCountry);
        $stmt->bindValue(':countryName', $countryName);
        return $stmt->execute();
    }

    /**
     * Verifica existencia de ciudad
     *
     * @method boolean cityExist()
     * @access public
     * @param string $country
     * @param string $city
     * @return boolean
     */
    public function cityExist($country, $city)
    {
        $stmt = $this->_db->prepare(self::$cityExist);
        $stmt->bindValue(':country', $country);
        $stmt->bindValue(':city', $city);
        $stmt->execute();
        $cityExist = $this->_functions->fetchUnset($stmt->fetch());
        return $cityExist['exist'];
    }

    /**
     * Inserta ciudad
     *
     * @method lastInsertId citySet()
     * @access public
     * @param string $country
     * @param string $city
     * @return lastInsertId
     */
    public function citySet($country, $city)
    {
        $stmt = $this->_db->prepare(self::$citySet);
        $stmt->bindValue(':country', $country);
        $stmt->bindValue(':city', $city);
        $stmt->execute();
        return $this->_db->lastInsertId();
    }

    /**
     * Trae id de ciudad
     *
     * @method string idCityGet()
     * @access public
     * @param string $country
     * @param $city
     * @return boolean
     */
    public function idCityGet($country, $city)
    {
        $stmt = $this->_db->prepare(self::$idCityGet);
        $stmt->bindValue(':country', $country);
        $stmt->bindValue(':city', $city);
        $stmt->execute();
        $idCity = $this->_functions->fetchUnset($stmt->fetch());
        return $idCity['id_cit'];
    }

    /**
     * Verifica existencia de región
     *
     * @method boolean regionExist()
     * @access public
     * @param string $city
     * @param string $region
     * @return boolean
     */
    public function regionExist($city, $region)
    {
        $stmt = $this->_db->prepare(self::$regionExist);
        $stmt->bindValue(':city', $city);
        $stmt->bindValue(':region', $region);
        $stmt->execute();
        $regionExist =  $this->_functions->fetchUnset($stmt->fetch());
        return $regionExist['exist'];
    }

    /**
     * Inserta region
     *
     * @method lastInsertId regionSet()
     * @access public
     * @param string $city
     * @param string $region
     * @param string $information
     * @return lastInsertId
     */
    public function regionSet($city, $region, $information)
    {
        $stmt = $this->_db->prepare(self::$regionSet);
        $stmt->bindValue(':city', $city);
        $stmt->bindValue(':region', $region);
        $stmt->bindValue(':cp', $information['zip']);
        $stmt->bindValue(':timeZone', $information['timeZone']);
        $stmt->bindValue(':latitude', $information['latitude']);
        $stmt->bindValue(':longitude', $information['longitude']);
        $stmt->execute();
        return $this->_db->lastInsertId();
    }

    /**
     * Trae id de region
     *
     * @method string idRegionGet()
     * @access public
     * @param string $city
     * @param string $region
     * @return boolean
     */
    public function idRegionGet($city, $region)
    {
        $stmt = $this->_db->prepare(self::$idRegionGet);
        $stmt->bindValue(':city', $city);
        $stmt->bindValue(':region', $region);
        $stmt->execute();
        return $this->_functions->fetchUnset($stmt->fetch());
    }

    /**
     * verifica existencia de ipXregion
     *
     * @method boolean ipXregionExist()
     * @access public
     * @param string $region
     * @param string $ipLower
     * @param string $ipHigher
     * @return boolean
     */
    public function ipXregionExist($region, $ipLower, $ipHigher)
    {
        $stmt = $this->_db->prepare(self::$ipXregionExist);
        $stmt->bindValue(':region', $region);
        $stmt->bindValue(':ipLower', $ipLower);
        $stmt->bindValue(':ipHigher', $ipHigher);
        $stmt->execute();
        $regionE = $this->_functions->fetchUnset($stmt->fetch());
        return $regionE['exist'];
    }

    /**
     * Inserta ipXregion
     *
     * @method lastInsertId ipXregionSet()
     * @access public
     * @param string $region
     * @param string $ipLower
     * @param string $ipHigher
     * @return lastInsertId 
     */
    public function ipXregionSet($region , $ipLower, $ipHigher)
    {
        $stmt = $this->_db->prepare(self::$ipXregionSet);
        $stmt->bindValue(':region', $region);
        $stmt->bindValue(':  ', $ipLower);
        $stmt->bindValue(':ipHigher', $ipHigher);
        $stmt->execute();
        return $this->_db->lastInsertId();
    }
}