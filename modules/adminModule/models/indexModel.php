<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @category Core
 */

namespace adminModule\Models;

class indexModel extends \Core\Model
{

	/**
	 * Trae información de modulos y metodos
	 *
	 * @name $modulesXmethodsGet
	 * @type SQL
	 * @access private
	 */
	private static $modulesXmethodsGet = <<<SQL
	SELECT
		MO.id_mod, MO.name_mod, MO.url_mod, MO.description_mod, MO.active_mod, MXP.id_mxp, MXP.profile_mxp, ME.id_met, ME.url_met, ME.description_met, ME.active_met, ME.name_met, MXE.id_mtp, MXE. profile_mtp

	FROM
		modules MO

	INNER JOIN
		modulesXprofiles MXP

		ON
			MXP.module_mxp = MO.id_mod

	INNER JOIN
		methods ME

		ON
			ME.module_met = MO.id_mod

	INNER JOIN
		methodsXprofiles MXE

		ON
			MXE.method_mtp = ME.id_met
SQL;

    public function __construct()
    {
		parent::__construct();
    }

	/**
	 * Trae información de modulos y metodos
	 *
	 * @method array modulesXmethodsGet()
	 * @access public
	 * @return array
	 */
	public function modulesXmethodsGet()
	{
		$stmt = $this->_db->prepare(self::$modulesXmethodsGet);
        $stmt->execute();
		$modules = $this->_functions->fetchAllUnset($stmt->fetchAll());
		$mod = [];
		foreach($modules AS $value)
		{
			$mod[$value['url_mod']]['name'] = $value['name_mod'];
			$mod[$value['url_mod']]['id'] = $value['id_mod'];
			$mod[$value['url_mod']]['description'] = $value['description_mod'];
			$mod[$value['url_mod']]['active'] = $value['active_mod'];
			$mod[$value['url_mod']]['profile'][$value['id_mxp']] = $value['profile_mxp'];
			$mod[$value['url_mod']]['method'][$value['url_met']]['name'] = $value['name_met'];
			$mod[$value['url_mod']]['method'][$value['url_met']]['id'] = $value['id_met'];
			$mod[$value['url_mod']]['method'][$value['url_met']]['description'] = $value['description_met'];
			$mod[$value['url_mod']]['method'][$value['url_met']]['active'] = $value['active_met'];
			$mod[$value['url_mod']]['method'][$value['url_met']]['profile'][$value['id_mtp']] = $value['profile_mtp'];
		}
		return $mod;
	}
}