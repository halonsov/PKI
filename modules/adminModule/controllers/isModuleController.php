<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @category Core
 */

namespace adminModule\Controllers;
use SimpleXMLElement;

class isModuleController extends \Core\Controller
{
    private $_modules = [];

    private $_folders = [];

    public function __construct()
    {
        parent::__construct();
        $this->_folders = $this->folderRead();
    }

    public function index() {}

    /**
     * Lee las carpetas contenidas en MODULES_DIR
     *
     * @method array folderRead()
     * @access private
     * @return array
     */
    private function folderRead()
    {
        $d = dir(MODULES_DIR);
        $dir = [];
        while ($entry = $d->read())
        {
            $dir[] = $entry;
        }
        $d->close();
        return $dir;
    }

    /**
     * Determina si el nombre de la carpeta tiene la estrutura de modulo
     *
     * @method boolean isModuleFolder()
     * @access private
     * @param string $module
     * @return boolean
     */
    private function isModuleFolder($module = FALSE)
    {
        if( !isset($module) OR $module == '' OR is_array($module) )
        {
            return false;
        }
        if ( $this->_functions->expressionValidate('module', $module) )
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determina si tiene los archivos necesarios para ser modulo
     *
     * @method boolean isModuleFiles
     * @access private
     * @param string $module
     * @return boolean
     */
    private function isModuleFiles($module = FALSE)
    {
        if( !isset($module) OR $module == '' OR is_array($module))
        {
            return false;
        }

        if(file_exists(MODULES_DIR.DS.$module.DS))
        {
            if( file_exists(MODULES_DIR.DS.$module.DS.CONTROLLER_DIR.DS) AND file_exists(MODULES_DIR.DS.$module.DS.MODEL_DIR) AND file_exists(MODULES_DIR.DS.$module.DS.VIEW_DIR) AND file_exists(MODULES_DIR.DS.$module.DS.'methods.xml'))
            {
                if( file_exists(MODULES_DIR.DS.$module.DS.GENERAL_CONTROLLER.'Module.php') AND
                    file_exists(MODULES_DIR.DS.$module.DS.CONTROLLER_DIR.DS.DEFAULT_CONTROLLER.'Controller.php') AND
                    file_exists(MODULES_DIR.DS.$module.DS.MODEL_DIR.DS.DEFAULT_MODEL.'Model.php') )
                {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Determina si una carpeta es modulo
     *
     * @method boolean isModule()
     * @access public
     * @return boolean
     */
    private function isModule($module = FALSE)
    {
        if( !isset($module) OR $module == '' OR is_array($module) )
        {
            return false;
        }

        if( !$this->isModuleFolder($module) )
        {
            return false;
        }

        if( !$this->isModuleFiles($module) )
        {
            return false;
        }

        return true;
    }

    /**
     * Determina la existencia de un modulo (methods.xml)
     *
     * @method array isModuleXml()
     * @access private
     * @param array $module
     * @return array
     */
    private function isModuleXml($module = FALSE)
    {
        if( !isset($module) OR $module == '' OR is_array($module) )
        {
            return false;
        }

        if( !file_exists(MODULES_DIR.DS.$module.DS.'methods.xml') )
        {
            return false;
        }

        $mod = [];
        $xml = new SimpleXMLElement(file_get_contents(MODULES_DIR.DS.$module.DS.'methods.xml'));
        if( !isset($xml->info->name) OR $xml->info->name == '')
        {
            return false;
        }
        $mod['name'] = (string) $xml->info->name;

        if( !isset($xml->info->url) OR $xml->info->url == '')
        {
            return false;
        }
        $mod['url'] = (string) $xml->info->url;

        if( !isset($xml->info->description) OR $xml->info->description == '')
        {
            return false;
        }
        $mod['description'] = (string) $xml->info->description;

        if( !isset($xml->methods) OR $xml->methods == '')
        {
            return false;
        }

        foreach($xml->methods->method AS $value)
        {
            if( !isset($value->url) OR $value->url == '')
            {
                continue;
            }

            if( !isset($value->description) OR $value->description == '')
            {
                continue;
            }

            if( !isset($value->profiles->profile) OR $value->profiles->profile == '')
            {
                continue;
            }

            $mod['methods'][(string) $value->name]['url'] = (string) $value->url;
            $mod['methods'][(string) $value->name]['description'] = (string) $value->description;
            $mod['methods'][(string) $value->name]['profiles'] = (array) $value->profiles->profile;
        }
        return $mod;
    }

    /**
     * Regresa los modulos y sus permisos de los archivos methods.xm
     *
     * @method array modulesXmlGet
     * @access public
     * @return array
     */
    public function modulesXmlGet()
    {
        $module = [];
        foreach($this->_folders AS $value)
        {
            if( !$this->isModule($value) )
            {
                continue;
            }

            if( !$this->isModuleFiles($value) )
            {
                continue;
            }

            $xml = $this->isModuleXml($value);
            if( !$xml)
            {
                continue;
            }
            $module[$xml['url']] = $xml;
        }
        return $module;
    }

    /**
     * Mezcla los modulosXML y los modulos de la base de datos
     *
     * @method array modulesMerge()
     * @access public
     * @param array $moduleXML
     * @param array $moduleDB
     * @return array
     */
    public function modulesMerge($moduleXML = FALSE, $moduleDB = FALSE)
    {
        if( !isset($moduleXML) OR !is_array($moduleXML) )
        {
            $moduleXML = $this->modulesXmlGet();
        }

        if( !isset($moduleDB) OR !is_array($moduleDB) )
        {
            $moduleDB = $this->_model->modulesXmethodsGet();
        }

        foreach($moduleXML AS $key => $value)
        {
            if( !isset($moduleDB[$key]) OR !is_array($moduleDB[$key]) )
            {
                $moduleDB[$key] = $value;
                continue;
            }
        }

        return $moduleDB;
    }

    /**
     * Regresa los modulos actuales y sus permisos
     *
     * @method array modulesSet
     * @access public
     * @return array
     */
    public function modulesSet()
    {
        return $this->modulesMerge();
    }
}