<script type="text/javascript">
    $(document).ready(function() {
        $('#paisselect').change(function(){
            var pais = $(this).val();
            $.ajax({
                url: "/ajax/worldGet/ciudad/"+pais,
                success: function(data) {
                    $("#ciudadselect").html(data);
                    $("#regionselect").html('<option value="" selected>---</option>');
                }
            });
        });
        $('#ciudadselect').change(function(){
            var ciudad = $(this).val();
            $.ajax({
                url: "/ajax/worldGet/region/"+ciudad,
                success: function(data) {
                    $("#regionselect").html(data);
                }
            });
        });
        $('#valor').change(function(){
            if ($('#campo').val() == 1)
            {
                usserValidate('correo', '#valor');
            } else if ($('#campo').val() == 2) {
                usserValidate('usuario', '#valor');
            }
        });
        $('#campo').change(function(){
            $('#valor').val('');
        });
    });
</script>
<div class="small-12 large-12 columns">
    <form id="formulario_usuario" method="POST" name="formulario_usuario" action="<?= SECURE_BASE_URL."users/recovers";?>">
        <div class="small-12 large-6 large-centered columns">
            <div class='large-offset-3 large-3 columns small-12'>
                <p>Campo:</p>
            </div>
            <div class='large-5 columns large-uncentered small-12'>
                <select id="campo" name="campo" data-validation="number" data-validation-error-msg="select">
					<option value="" selected>---</option>
                    <option value="1" selected>Correo</option>
                    <option value="2" selected>Usuario</option>
                </select>
            </div>
        </div>
        <div class="small-12 large-6 large-centered columns">
            <div class='large-offset-3 large-3 columns small-12'>
                <p>Valor:</p>
            </div>
            <div class='large-5 columns large-uncentered small-12'>
                <input id="valor" name="valor" type="text" placeholder="[a-zA-Z0-9@]" data-validation="length" data-validation-length="1-200" data-validation-error-msg="confirmation">
            </div>
        </div>
        <div class='small-12 large-1 large-centered columns'>
            <button class="submit" type="submit">Submit</button>
        </div>
    </form>
    <script>
        $.validate({
            modules : 'date, security'
        });
    </script>
</div>