<script>
    $(document).ready(function(){
        var nombreAnterior = $('#nombreIG').val();
        var apellidoAnterior = $('#nombreIG').val();
        var fNacimientoAnterior = $('#datepickerUSR').val();
        var sexoAnterior = $('#sexsselect').val();
        var googleAnterior = $('#googlePLUS').val();
        var twitterAnterior = $('#twitterUSR').val();
        var facebookAnterior = $('#facebookUSR').val();
        var biografiaAnterior = $('biografiaUSR').val();
        $('#c_anterior').change(function(){
            usserValidate('contrasenia', '#c_anterior')
        });
        $('#nombreIG').change(function(){
            var parametros = {
	            "nameUSR" : $('#nombreIG').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/name",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#nombreIG').val(nombreAnterior);
                        $('#nombreIG').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        nombreAnterior = $('#nombreIG').val();
                        $('#nombreIG').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#apellidosIG').change(function(){
            var parametros = {
	            "lastNameUSR" : $('#apellidosIG').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/lastName",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#apellidosIG').val(apellidoAnterior);
                        $('#apellidosIG').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        apellidoAnterior = $('#apellidosIG').val();
                        $('#apellidosIG').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#datepickerUSR').change(function(){
            var parametros = {
	            "birthdateUSR" : $('#datepickerUSR').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/fnacimiento",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#datepickerUSR').val(fNacimientoAnterior);
                        $('#datepickerUSR').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        fNacimientoAnterior = $('#datepickerUSR').val();
                        $('#datepickerUSR').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $("#sexsselect option[value='"+<?= $_SESSION['sexUSR'] ?>+"']").attr("selected","selected");
        $('#sexsselect').change(function(){
            var parametros = {
	            "sexUSR" : $('#sexsselect').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/sex",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#sexsselect').val(fNacimientoAnterior);
                        $('#sexsselect').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        fNacimientoAnterior = $('#sexsselect').val();
                        $('#sexsselect').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#googlePLUS').change(function(){
            var parametros = {
	            "googleUSR" : $('#googlePLUS').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/google",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#googlePLUS').val(fNacimientoAnterior);
                        $('#googlePLUS').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        googleAnterior = $('#googlePLUS').val();
                        $('#googlePLUS').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#twitterUSR').change(function(){
            var parametros = {
	            "twitterUSR" : $('#twitterUSR').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/twitter",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#twitterUSR').val(twitterAnterior);
                        $('#twitterUSR').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        twitterAnterior = $('#twitterUSR').val();
                        $('#twitterUSR').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#facebookUSR').change(function(){
            var parametros = {
	            "facebookUSR" : $('#facebookUSR').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/facebook",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#facebookUSR').val(facebookAnterior);
                        $('#facebookUSR').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        facebookAnterior = $('#facebookUSR').val();
                        $('#facebookUSR').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
        $('#biografiaUSR').change(function(){
            var parametros = {
	            "biografiaUSR" : $('#biografiaUSR').val(),
	        };
            $.ajax({
                data:  parametros,
                url: "/users/change/biografia",
                type:  'post',
                success: function(data) {
                    if (data == 0)
                    {
                        $('#biografiaUSR').val(biografiaAnterior);
                        $('#biografiaUSR').attr('style', 'border-color: red; color: red;');
                    } else if (data == 1) {
                        biografiaAnterior = $('#biografiaUSR').val();
                        $('#biografiaUSR').attr('style', 'border-color: #43AC6A; color: #43AC6A;');
                    }
                }
            });
        });
    });
</script>
<dl class="accordion" data-accordion>
    <dd>
        <a href="#panel1">Cambiar Contraseña</a>
        <div id="panel1" class="content active small-12 large-12 columns">
            <form id="cambia_contrasenia" method="POST" name="cambia_contrasenia" action="<?= SECURE_BASE_URL."users/change/contrasenia";?>">
                <div class="small-12 large-7 large-centered columns">
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Contraseña Anterior:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="c_anterior" name="c_anterior" type="password" placeholder="8-12 [a-zA-Z0-9!@#]" maxlength="12" data-validation="length" data-validation-length="8-12">
                    </div>
                </div>
                <div class="small-12 large-7 large-centered columns">
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Contraseña:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="contrasenia_confirmation" name="contrasenia_confirmation" type="password" placeholder="8-12 [a-zA-Z0-9!@#]" maxlength="12" data-validation="length" data-validation-length="8-12" data-validation-error-msg="confirmation">
                    </div>
                </div>
                <div class="small-12 large-7 large-centered columns">
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Confirmar:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="contrasenia" name="contrasenia" type="password" placeholder="Reingresa la contraseña" maxlength="12" data-validation="confirmation" data-validation-error-msg="confirmation">
                    </div>
                </div>
                <div class='small-12 large-1 large-centered columns'>
                    <button class="submit" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </dd>
    <dd>
        <a href="#panel2">Información Usuario</a>
        <div id="panel2" class="content small-12 large-12 columns">
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Nombre:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="nombreIG" name="nombreIG" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['nameUSR']?>">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Apellidos:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="apellidosIG" name="apellidosIG" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['lastNameUSR'] ?>">
                    </div>
                </div>
            </div>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Sexo:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <select id="sexsselect" name="sexsselect" data-validation="number" data-validation-error-msg="select">
                            <?= $this->sexs ?>
                        </select>
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Fecha de nacimiento:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="datepickerUSR" name="fnacimiento" placeholder="aaaa-mm-dd" maxlength="12" type="date" data-validation="birthdate" data-validation-error-msg="birthdate" value='<?= $_SESSION['birthdateUSR'] ?>'>
                    </div>
                </div>
            </div>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Imágen:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="imagen" name="imagen" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['nameUSR']?>">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Google+:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="googlePLUS" name="googlePLUS" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['googleUSR'] ?>">
                    </div>
                </div>
            </div>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Twitter:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="twitterUSR" name="twitterUSR" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['twitterUSR']?>">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Facebook:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="facebookUSR" name="facebookUSR" type="text" placeholder="[A-Za-z]" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="alpha" value="<?= $_SESSION['facebookUSR'] ?>">
                    </div>
                </div>
            </div>
        </div>
    </dd>
    <dd>
        <a href="#panel3">Información biográfica</a>
        <div id="panel3" class="content">
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-12 columns'>
                    <div class='large-offset-1 large-2 columns small-12'>
                        <p>Información:</p>
                    </div>
                    <div class='large-8 columns large-uncentered small-12'>
                        <textarea name="biografiaUSR" id="biografiaUSR" rows="10" style="resize:none" placeholder="Información biográfica" ><?= $_SESSION['generalInfoUSR'] ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </dd>
</dl>
<script>
    $(function(){$( "#datepickerUSR" ).datepicker();});
    $.validate({
        modules : 'date, security'
    });
</script>