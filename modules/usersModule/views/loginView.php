<div class="small-12 large-12 columns">
    <form id="formulario_usuario" method="POST" name="formulario_usuario" action="<?= SECURE_BASE_URL."users/loguea";?>">
        <div class="small-12 large-6 large-centered columns">
            <div class='large-offset-3 large-3 columns small-12'>
                <p>Usuario:</p>
            </div>
            <div class='large-5 columns large-uncentered small-12'>
                <input id="user" name="user" type="text" placeholder="[A-Za-z0-9]" maxlength="25" data-validation="usser" data-validation-error-msg="usser">
            </div>
        </div>
        <div class="small-12 large-6 large-centered columns">
            <div class='large-offset-3 large-3 columns small-12'>
                <p>Contraseña:</p>
            </div>
            <div class='large-5 columns large-uncentered small-12'>
                <input id="contrasenia" name="contrasenia" type="password" placeholder="8-12 [a-zA-Z0-9!@#]" maxlength="12" data-validation="length" data-validation-length="8-12" data-validation-error-msg="confirmation">
            </div>
        </div>
        <div class='small-12 large-1 large-centered columns'>
            <button class="submit" type="submit">Submit</button>
        </div>
    </form>
    <script>
        $.validate({
            modules : 'date, security'
        });
    </script>
</div>