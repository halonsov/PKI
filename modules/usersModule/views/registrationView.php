<div class="small-12  large-12 large-centered columns">
	<form id="formulario_usuario" method="POST" name="formulario_usuario" action="<?= SECURE_BASE_URL."users/registration/registration";?>">
		<div class="row">
            <div class="small-12 large-5 large-centered columns">
                <h1>Formulario de registro</h1>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Nombre:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="name" name="name" type="text" placeholder="[A-Za-z]" maxlength="30" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="Solo letras, este campo no debe estar vacío">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Apellidos:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="lastName" name="lastName" type="text" placeholder="[A-Za-z]" maxlength="30" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="Solo letras, este campo no debe estar vacío">
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>País:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <select id="paisselect" name="paisselect" data-validation="alpha" data-validation-allowing=" " data-validation-error-msg="Seleccione una opción">
							<option value="" selected>---</option>
                            <?= $this->countries ?>
                        </select>
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Ciudad:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <select id="ciudadselect" name="ciudadselect" data-validation="number" data-validation-error-msg="Seleccione una opción">
							<option value="" selected>---</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Región:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <select id="regionselect" name="regionselect" data-validation="number" data-validation-error-msg="Seleccione una opción">
							<option value="" selected>---</option>
                        </select>
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Fecha de nacimiento:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="datepicker" name="fnacimiento" placeholder="aaaa-mm-dd" maxlength="12" type="date" data-validation="birthdate" data-validation-error-msg="Ingresa tu fecha de nacimiento">
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Usuario:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="user" name="user" type="text" placeholder="[A-Za-z0-9]" maxlength="25" data-validation="usser" data-validation-error-msg="Ingresa tu nombre de ususario">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>E-Mail:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="correo" name="correo" placeholder="Un correo activo" maxlength="50" type="text" data-validation="correo" data-validation-error-msg="Ingresa tu correo">
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Contraseña:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="contrasenia_confirmation" name="contrasenia_confirmation" type="password" placeholder="8-12 [a-zA-Z0-9!@#]" maxlength="12" data-validation="length" data-validation-length="8-12" data-validation-error-msg="Ingresa una contraseña">
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                        <p>Confirmar:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <input id="contrasenia" name="contrasenia" type="password" placeholder="Reingresa la contraseña" maxlength="12" data-validation="confirmation" data-validation-error-msg="Las contraseñas no coinciden">
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-3 large-3 columns small-12'>
                        <p>Sexo:</p>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                        <select id="sexsselect" name="sexsselect" data-validation="number" data-validation-error-msg="Seleccione una opción">
							<option value="" selected>---</option>
							<?= $this->sexs ?>
						</select>
                    </div>
                </div>
                <div class='small-12 large-6 columns'>
                    <div class='large-offset-1 large-3 columns small-12'>
                    </div>
                    <div class='large-5 columns large-uncentered small-12'>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='small-12 large-12 columns'>
                <div  class='small-12 large-6 large-centered columns'>
                    <div  class='small-12 large-9 large-centered columns'>
                        <input id="check" name="check" type="checkbox" required />
                        Soy mayor de 18 años y he leído y aceptado los <a href="/media/Terminos_y_Condiciones.pdf" target="_blank">Términos y Condiciones</a> .
                        Acepto recibir informacion de PKI, podré cancelar este servicio dentro de mi cuenta en cualquier momento.
                        <a href="/media/Terminos_y_Condiciones.pdf" target="_blank">Leer Términos y Condiciones</a>
                    </div>
					<div  class='small-12 large-6 large-centered columns'>
                    </div>
				</div>
                <div class='small-12 large-1 large-centered columns'>
                    <button class="submit" type="submit">Submit</button>
                </div>
            </div>
			</div>
        </div>
	</form>
    <script>
        $.validate({
            modules : 'date, security'
        });
    </script>
</div>