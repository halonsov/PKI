<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule;

class mainModule extends \Core\Module
{

    private static $calendario = '$(function() {$( "#datepicker" ).datepicker();});';

    public function __construct(\Core\Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $this->module404Load();
        exit;
    }

    /**
	 * Registro usuario
	 *
	 * @method void registration() Carga el formulario de registro
	 * @access public
	 * @return void
	 */
    public function registration($register = FALSE)
    {
        $this->methodPermissions('registration');
        if( isset($register) AND $register == 'registration')
        {
            if( !isset($_POST) )
            {
                $_SESSION['errors']->errorAdd('No está definida la variable global $this->_functions->callPost() : '.__FILE__.' ('.__LINE__.')');
                $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No se pudo registrar, intentelo mas tarde']);
                exit;
            } else {
                $registration = $this->controllerLoad('registration');
                if($registration->ussersRegistration($this->_functions->callPost()))
                {
                    $this->moduleDefaultLoad(['type' => 'success', 'message' => 'Registro exitoso']);
                    exit;
                } else {
                    $_SESSION['errors']->errorAdd('Ocurrió un error al tratar de registrar el usuario : '.__FILE__.' ('.__LINE__.')');
                    $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No se pudo registrar, intentelo mas tarde']);
                    exit;
                }
            }
        } else {
            $this->_view->scriptsEmbeddedSet(
                [
                    'js'    =>  [
                        '$(function() {$( "#datepicker" ).datepicker();});',
                        '$(document).ready(function() {
                            $(\'#paisselect\').change(function(){
                                var pais = $(this).val();
                                $.ajax({
                                    url: "/ajax/worldGet/ciudad/"+pais,
                                    success: function(data) {
                                        $("#ciudadselect").html(data);
                                        $("#regionselect").html(\'<option value="" selected>---</option>\');
                                    }
                                });
                            });
                            $(\'#ciudadselect\').change(function(){
                                var ciudad = $(this).val();
                                $.ajax({
                                    url: "/ajax/worldGet/region/"+ciudad,
                                    success: function(data) {
                                        $("#regionselect").html(data);
                                    }
                                });
                            });
                            $(\'#user\').change(function(){usserExist(\'usuario\', \'#user\')});
                            $(\'#correo\').change(function(){usserExist(\'correo\', \'#correo\')});
                        });'
                    ]
                ]);
            $registration = $this->controllerLoad('registration');
            $this->pageTitleSet('registration');
            $this->_view->sexs = $registration->sexsOptionGet();
            $this->_view->countries = $registration->countriesOptionGet();
            $this->_view->render('registration');
            exit;
        }
    }

    /*
     * Activa usuario
     *
     * @method void active() Activa usuario
     * @access public
     * @return void
     */
    public function active($usser = FALSE, $confirmation = FALSE)
    {
        $this->methodPermissions('active');
        if( !isset($usser) OR $usser == '' )
        {
            $_SESSION['errors']->errorAdd('No esta definida la variable $usser : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad();
            exit;
        }

		if( !isset($confirmation) OR $confirmation == '')
		{
			$_SESSION['errors']->errorAdd('No esta definida la variable $confirmation : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad();
            exit;
		}

		$validate = $this->controllerLoad('validate');
        if($validate->usserExist($usser) == 1)
        {
			$uss = $validate->usserIdGet($usser);
			$conf = md5(md5(md5(md5($uss['usser']))).md5(md5(md5(md5($uss['password'])))).md5(md5(md5(md5(md5(md5(md5($uss['name'].' '.$uss['lastName']))))))));
			if($confirmation != $conf)
			{
				$_SESSION['errors']->errorAdd('No coinciden los códigos de confirmación : '.__FILE__.' ('.__LINE__.')');
				$this->moduleDefaultLoad();
				exit;
			}
            $validate->usserActive($usser);
            $this->moduleDefaultLoad(['type' => 'success', 'message' => 'Tu usuario ha sido activado con éxito']);
            exit;
        } else {
            $_SESSION['errors']->errorAdd('No existe el usuario '.$usser.' : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No existe el usuario']);
            exit;
        }
    }

    /*
     * Carga el formulario de login
     *
     * @method void login() Carga el formulario de login
     * @access public
     * @return void
     */
    public function login()
    {
        $this->methodPermissions('login');
        $this->pageTitleSet('login');
        $this->_view->render("login");
        exit;
    }

    /*
     * Loguea usuario
     *
     * @method void loguea() Loguea usuario
     * @access public
     * @return void
     */
    public function loguea()
    {
        $this->methodPermissions('loguea');
        if( !isset($_POST) )
        {
            $_SESSION['errors']->errorAdd('No está definida la variable global $this->_functions->callPost() : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No se pudo registrar, intentelo mas tarde']);
            exit;
        } else {
            $login = $this->controllerLoad("login");
            $login = $login->login($this->_functions->callPost());
            if($login == 2)
            {
                $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'Usuario inactivo']);
                exit;
            } elseif($login == 3){
                $this->moduleDefaultLoad(['type' => 'success', 'message' => 'Login exitoso']);
                exit;
            } else {
                $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No existe usuario']);
                exit;
            }
        }
    }

    /*
     * Cambia parametros del usuario
     *
     * @method void change() Cambia parametros del usuario
     * @access public
     * @return void
     */
    public function change($parameter = FALSE)
    {
        $this->methodPermissions('change');
        if( !\Core\Session::get('isAuthUSR'))
        {
            $this->moduleDefaultLoad();
            exit;
        }

        if( !isset($_POST) )
        {
            $_SESSION['errors']->errorAdd('Se esperaba un array por $this->_functions->callPost() : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'Ocurrió un error al cambiar contraseña']);
            exit;
        }

        if( !isset($parameter) OR $parameter == '')
        {
            $_SESSION['errors']->errorAdd('$parameter no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'Ocurrió un error al cambiar contraseña']);
            exit;
        }

        $change = $this->controllerLoad("change");
        if($parameter == 'contrasenia')
        {
            if($change->passChange($this->_functions->callPost()))
            {
                $this->moduleDefaultLoad(['type' => 'success', 'message' => 'Se cambió la contraseña']);
                exit;
            } else {
                $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'Ocurrió un error al cambiar la contraseña intente mas tarde']);
                exit;
            }
        } elseif($parameter == 'name') {
            if($change->nameChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'lastName') {
            if($change->lastNameChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'fnacimiento') {
            if($change->birthdayChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'sex') {
            if($change->sexChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'google') {
            if($change->googleChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'twitter'){
            if($change->twitterChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'facebook') {
            if($change->facebookInsert($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } elseif($parameter == 'biografia') {
            if($change->biographyChange($this->_functions->callPost()))
            {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    /*
     * Cierra sesion
     *
     * @method void logout()  Cierra sesion
     * @access public
     * @return void
     */
    public function logout()
    {
        $this->methodPermissions('logout');
        if( isset($_SESSION['isAuthUSR']) )
        {
            $login = $this->controllerLoad("login");
            $login->logout();
        }
        $this->moduleDefaultLoad();
        exit;
    }

    /*
     * Formulario de recuperar contraseña
     *
     * @method void recover() Formulario de recuperar contraseña
     * @access public
     * @return void
     */
    public function recover()
    {
        $this->methodPermissions('recover');
        $this->_view->render('recover');
        exit;
    }

    /*
     * Recuperar contraseña
     *
     * @method void recover() Recuperar contraseña
     * @access public
     * @return void
     */
    public function recovers()
    {
        $this->methodPermissions('recovers');
        if( \Core\Session::get('isAuthUSR') )
        {
            $login = $this->controllerLoad("login");
            $login->logout();
        }
        unset($login);

        if( !isset($_POST) )
        {
            $this->moduleDefaultLoad();
            exit;
        }

        $recover = $this->controllerLoad('recover');
        if($recover->recover($this->_functions->callPost()))
        {
            $this->moduleDefaultLoad(['type' => 'success', 'message' => 'Se enviaron tus datos por correo']);
            exit;
        } else {
            $this->moduleDefaultLoad(['type' => 'warning', 'message' => 'No existe usuario']);
            exit;
        }
    }

    /**
	 * Muestra perfil del usuario
	 * @method void profile()
	 * @access public
	 * @return void
     */
    public function profile()
    {
        $registration = $this->controllerLoad('registration');
        $this->methodPermissions('profile');
        $this->_view->sexs = $registration->sexsOptionGet();
        $this->_view->render('profile');
        exit;
    }
}