<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Models;

/**
 * Modelo para cargar página de inicio
 */
class indexModel extends \Core\Model
{
    /**
     * Obtiene sexos
     *
     * @name $sexGet
     * @type SQL
     * @access private
     */
    private static $sexGet = <<<SQL
    SELECT
        id_sex, sex_sex

    FROM
        sexs
SQL;

    /*
     * Obtiene paises
     *
     * @name $countriesGet
     * @type SQL
     * @access private
     */
    private static $countriesGet = <<<SQL
    SELECT
        id_cou, name_cou

    FROM
        countries
SQL;

    /*
     * Verifica existencia de un país
     *
     * @name $countryExist
     * @type SQL
     * @access private
     */
    private static $countryExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        countries

    WHERE
        id_cou = :country
SQL;

    /*
     * Verifica existencia de ciudad
     *
     * @name $cityExist
     * @type SQL
     * @access private
     */
    private static $cityExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        cities

    WHERE
        id_cit = :city
SQL;

    /*
     * Verifica existencia de region
     *
     * @name $regionExist
     * @type SQL
     * @access private
     */
    private static $regionExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        regions

    WHERE
        id_reg = :region
SQL;


    /*
     * Verifica existencia de nombre de usuario
     *
     * @name $usserNameExist
     * @type SQL
     * @access private
     */
    private static $usserNameExist = <<<SQL
        SELECT
            count(*) AS exist
    
        FROM
            ussers
    
        WHERE
            usser_usr = :usser
SQL;

    /*
     * Verifica existencia de correo
     *
     * @name $usserMailExist
     * @type SQL
     * @access private
     */
    private static $usserMailExist = <<<SQL
    SELECT
        count(*) AS exist

    FROM
        ussers

    WHERE
        mail_usr = :mail
SQL;


    /*
     * Verifica la existencia de usuario por id
     *
     * @name $usserIdExist
     * @type SQL
     * @access private
     */
    private static $usserIdExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        ussers

    WHERE
        id_usr = :usser
SQL;


    /*
     * Verifica existencia de sexo
     *
     * @name $sexExist
     * @type SQL
     * @access private
     */
    private static $sexExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        sexs

    WHERE
        id_sex = :sex
SQL;

    /*
     * Inserta usuario
     *
     * @name $usserSet
     * @type SQL
     * @access private
     */
    private static $usserSet = <<<SQL
    INSERT INTO
        ussers(
            name_usr, lastName_usr, birthday_usr, registration_usr, region_usr, sex_usr, usser_usr, password_usr, mail_usr, active_usr, profile_usr)

        VALUES (
            :name, :lastName, :birthday, NOW(), :region, :sex, :usser, :password, :mail, :active, :profile
        )
SQL;

    /*
     * Activa usuario
     *
     * @name $usserActive
     * @type SQL
     * @access private
     */
    private static $usserActive = <<<SQL
    UPDATE
        ussers

    SET
        active_usr = 1

    WHERE
        id_usr = :usser
SQL;

    /*
     * Trae información de usuario por nombre de usuario y contraseña
     *
     * @name $usserGet
     * @type SQL
     * @access private
     */
    private static $usserGet = <<<SQL
	SELECT
		US.id_usr, US.name_usr, US.lastName_usr, US.birthday_usr, US.registration_usr, US.region_usr, US.sex_usr, US.usser_usr, US.mail_usr, US.active_usr, US.profile_usr, EX.infGeneral_ext, EX.imgUsser_ext, EX.googleP_ext, EX.twitter_ext, EX.facebook_ext

	FROM
		ussers US

    LEFT JOIN
        ussersExtra EX

        ON
            EX.idUsser_ext = US.id_usr

	WHERE
		usser_usr = :usser AND password_usr = :password
SQL;

    /**
     * Trae información de usuario por id
     *
     * @name $usserIdGet
     * @type SQL
     * @access private
     */
    private static $usserIdGet = <<<SQL
    SELECT
		US.id_usr, US.password_usr, US.name_usr, US.lastName_usr, US.birthday_usr, US.registration_usr, US.region_usr, US.sex_usr, US.usser_usr, US.mail_usr, US.active_usr, US.profile_usr, EX.infGeneral_ext, EX.imgUsser_ext, EX.googleP_ext, EX.twitter_ext, EX.facebook_ext

	FROM
		ussers US

    LEFT JOIN
        ussersExtra EX

        ON
            EX.idUsser_ext = US.id_usr

	WHERE
		id_usr = :idUSR
SQL;

    /*
     * Verifica contraseña correcta de usuario por id
     *
     * @name $usserPassValidate
     * @type SQL
     * @access private
     */
    private static $usserPassValidate = <<<SQL
    SELECT
        count(*) AS exist

    FROM
        ussers

    WHERE
        id_usr = :usser AND password_usr = :password
SQL;

    /*
     * Actualiza conytraseña de usuario
     *
     * @name $passSet
     * @type SQL
     * @access private
     */
    private static $passSet = <<<SQL
    UPDATE
        ussers

    SET
        password_usr = :password

    WHERE
        id_usr = :usser
SQL;

    /*
     * Trae información de usuario por nombre de usuario
     *
     * @name $usserUsserGet
     * @type SQL
     * @access private
     */
    private static $usserUsserGet = <<<SQL
    SELECT id_usr, usser_usr, mail_usr

    FROM
        ussers

    WHERE
        usser_usr = :usser
SQL;

    /*
     * Cambia nombre
     *
     * @name $usserNameSet
     * @type SQL
     * @access public
     */
    private static $usserNameSet = <<<SQL
    UPDATE
        ussers

    SET
        name_usr = :name

    WHERE
        id_usr = :idUsr
SQL;

    /**
     * Cambia apellido de usuario
     *
     * @name $lastNameSet
     * @type SQL
     * @access private
     */
    private static $lastNameSet = <<<SQL
    UPDATE
        ussers

    SET
        lastName_usr = :lastName

    WHERE
        id_usr = :idUsr
SQL;

    /**
     * Cambia fecha de nacimiento de usuario
     *
     * @name $birthdaySet
     * @type SQL
     * @access private
     */
    private static $birthdaySet = <<<SQL
    UPDATE
        ussers

    SET
        birthday_usr = :birthday

    WHERE
        id_usr = :idUsr
SQL;

    /**
     * Cambia sexo de usuario
     *
     * @name $sexSet
     * @type SQL
     * @access private
     */
    private static $sexSet = <<<SQL
    UPDATE
        ussers

    SET
        sex_usr = :sex

    WHERE
        id_usr = :idUsr
SQL;

    /**
     * Cambia google+ de usuario
     *
     * @name $googleUsserSet
     * @type SQL
     * @access private
     */
    private static $googleUsserSet = <<<SQL
    UPDATE
        ussersExtra

    SET
        googleP_ext = :google

    WHERE
        idUsser_ext = :idUsr
SQL;

    /**
     * Valida existencia de información extra de usuario
     *
     * @name $usserExtraExist
     * @type SQL
     * @access private
     */
    private static $usserExtraExist = <<<SQL
    SELECT
        COUNT(*) AS exist

    FROM
        ussersExtra

    WHERE
        idUsser_ext = :usser
SQL;

    /**
     * Inserta google+ de usuario
     *
     * @name $googleUsserInsert
     * @type SQL
     * @access private
     */
    private static $googleUsserInsert = <<<SQL
    INSERT INTO
        ussersExtra
            (idUsser_ext, googleP_ext)

    VALUES
        (:usser, :google)
SQL;

    /**
     * Inserta twitter de usuario
     *
     * @name $twitterUsserInsert
     * @type SQL
     * @access private
     */
    private static $twitterUsserInsert = <<<SQL
    INSERT INTO
        ussersExtra
            (idUsser_ext, twitter_ext)

    VALUES
        (:usser, :twitter)
SQL;

    /**
     * Cambia twitter de usuario
     *
     * @name $twitterUsserSet
     * @type SQL
     * @access private
     */
    private static $twitterUsserSet = <<<SQL
    UPDATE
        ussersExtra

    SET
        twitter_ext = :twitter

    WHERE
        idUsser_ext = :usser
SQL;

    /**
     * Inserta facebook de usuario
     *
     * @name $facebookInsert
     * @type SQL
     * @access private
     */
    private static $facebookInsert = <<<SQL
    INSERT INTO
        ussersExtra
            (idUsser_ext, facebook_ext)

    VALUES
        (:usser, :facebook)
SQL;

    /**
     * Cambia facebook de usuario
     *
     * @name $facebookSet
     * @type SQL
     * @access private
     */
    private static $facebookSet = <<<SQL
    UPDATE
        ussersExtra

    SET
        facebook_ext = :facebook

    WHERE
        idUsser_ext = :usser
SQL;

    /**
     * Inserta Biografía de usuario
     *
     * @name $usserBiographyInsert
     * @type SQL
     * @access private
     */
    private static $usserBiographyInsert = <<<SQL
    INSERT INTO
        ussersExtra
            (idUsser_ext, infGeneral_ext)

    VALUES
        (:usser, :biography)
SQL;

    /**
     * Cambia biografía de usuario
     *
     * @name $biographySet
     * @type SQL
     * @access private
     */
    private static $biographySet = <<<SQL
    UPDATE
        ussersExtra

    SET
        infGeneral_ext = :biography

    WHERE
        idUsser_ext = :usser
SQL;

    public function __construct()
    {
		parent::__construct();
    }

    /**
     * Trae sexos
     *
     * @method array sexGet()
     * @access public
     * @return array
     */
    public function sexGet()
    {
        $stmt = $this->_db->prepare(self::$sexGet);
        $stmt->execute();
        $sexs = [];
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $sexs[$value['id_sex']] = $value['sex_sex'];
        }
        return $sexs;
    }

    /**
     * Trae paises
     *
     * @method array countriesGet()
     * @access public
     * @return array
     */
    public function countriesGet()
    {
        $stmt = $this->_db->prepare(self::$countriesGet);
        $stmt->execute();
        $countries = [];
        $i=0;
        foreach($this->_functions->fetchAllUnset($stmt->fetchAll()) AS $value)
        {
            $countries[$value['id_cou']] = $value['name_cou'];
        }
        return $countries;
    }

    /**
     * Verifica existencia de país
     *
     * @method boolean countryExist()
     * @access public
     * @param string $country
     * @return boolean
     */
    public function countryExist($country)
    {
        $stmt = $this->_db->prepare(self::$countryExist);
        $stmt->bindValue(':country', $country);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Verifica existencia de ciudad
     *
     * @method boolean cityExist()
     * @access public
     * @param string $city
     * @return boolean
     */
    public function cityExist($city)
    {
        $stmt = $this->_db->prepare(self::$cityExist);
        $stmt->bindValue(':city', $city);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Verifica existencia de region
     *
     * @method boolean regionExist()
     * @access public
     * @param string $region
     * @return boolean
     */
    public function regionExist($region)
    {
        $stmt = $this->_db->prepare(self::$regionExist);
        $stmt->bindValue(':region', $region);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Verifica existencia nombre de usuario
     *
     * @method usserNameExist
     * @access public
     * @param string $usser
     * @return boolean
     */
    public function usserNameExist($usser)
    {
        $stmt = $this->_db->prepare(self::$usserNameExist);
        $stmt->bindValue(':usser', $usser);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Verifica existencia de correo
     *
     * @method boolean usserMailExist()
     * @access public
     * @param string $mail
     * @return boolean
     */
    public function usserMailExist($mail)
    {
        $stmt = $this->_db->prepare(self::$usserMailExist);
        $stmt->bindValue(':mail', $mail);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Verifica existencia de sexo
     *
     * @method boolean sexExist()
     * @access public
     * @param string $sex
     * @return boolean
     */
    public function sexExist($sex)
    {
        $stmt = $this->_db->prepare(self::$sexExist);
        $stmt->bindValue(':sex', $sex);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Inserta usuario
     *
     * @method lastInsertId usserSet()
     * @access public
     * @param array $usser
     * @return lastInsertId
     */
    public function usserSet($usser)
    {
        $stmt = $this->_db->prepare(self::$usserSet);
        $stmt->bindValue(':name', $usser['name']);
        $stmt->bindValue(':lastName', $usser['lastName']);
        $stmt->bindValue(':birthday', $usser['birthdate']);
        $stmt->bindValue(':region', $usser['region']);
        $stmt->bindValue(':sex', $usser['sex']);
        $stmt->bindValue(':usser', $usser['usser']);
        $stmt->bindValue(':password', md5($usser['password']));
        $stmt->bindValue(':mail', $usser['mail']);
        $stmt->bindValue(':active', $usser['active']);
        $stmt->bindValue(':profile', $usser['profile']);
        $stmt->execute();
        return $this->_db->lastInsertId();
    }

    /**
     * Verifica existencia de id de usuario
     *
     * @method boolean usserIdExist()
     * @access public
     * @param string $usser
     * @return boolean
     */
    public function usserIdExist($usser)
    {
        $stmt = $this->_db->prepare(self::$usserIdExist);
        $stmt->bindValue(':usser', $usser);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
        return $exist['exist'];
    }

    /**
     * Activa usuario
     *
     * @method boolean usserActive()
     * @access public
     * @param string $usser
     * @return boolean
     */
    public function usserActive($usser)
    {
        $stmt = $this->_db->prepare(self::$usserActive);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Trae usuario apartir de usuario y contraseña
     *
     * @method array usserGet()
     * @access public
     * @param string $usser
     * @param string $password
     * @return array
     */
    public function usserGet($usser, $password)
	{
        $stmt = $this->_db->prepare(self::$usserGet);
        $stmt->bindValue(':usser', $usser);
        $stmt->bindValue(':password', md5($password));
        $stmt->execute();
		$us = $this->_functions->fetchAllUnset($stmt->fetchAll());
		if( !isset($us[0]) )
		{
			return '';
		}
		$us = $us[0];
        $usr = [];
		$usr['name'] = $us['name_usr'];
        $usr['lastName'] = $us['lastName_usr'];
		$usr['birthday'] = $us['birthday_usr'];
		$usr['registration'] = $us['registration_usr'];
		$usr['region'] = $us['region_usr'];
		$usr['sex'] = $us['sex_usr'];
		$usr['usser'] = $us['usser_usr'];
		$usr['mail'] = $us['mail_usr'];
		$usr['active'] = $us['active_usr'];
		$usr['profile'] = $us['profile_usr'];
		$usr['id'] = $us['id_usr'];
        $usr['infGeneral'] = $us['infGeneral_ext'];
        $usr['imgUsser'] = $us['imgUsser_ext'];
        $usr['googleP'] = $us['googleP_ext'];
        $usr['twitter'] = $us['twitter_ext'];
        $usr['facebook'] = $us['facebook_ext'];
		return $usr;
	}

    /**
     * Trae usuario apartir de id
     *
     * @method array usserGet()
     * @access public
     * @param string $usser
     * @param string $password
     * @return array
     */
    public function usserIdGet($id)
	{
        $stmt = $this->_db->prepare(self::$usserIdGet);
        $stmt->bindValue(':idUSR', $id);
        $stmt->execute();
		$us = $this->_functions->fetchAllUnset($stmt->fetchAll());
		if( !isset($us[0]) )
		{
			return '';
		}
		$us = $us[0];
        $usr = [];
		$usr['name'] = $us['name_usr'];
        $usr['lastName'] = $us['lastName_usr'];
		$usr['birthday'] = $us['birthday_usr'];
        $usr['password'] = $us['password_usr'];
		$usr['registration'] = $us['registration_usr'];
		$usr['region'] = $us['region_usr'];
		$usr['sex'] = $us['sex_usr'];
		$usr['usser'] = $us['usser_usr'];
		$usr['mail'] = $us['mail_usr'];
		$usr['active'] = $us['active_usr'];
		$usr['profile'] = $us['profile_usr'];
		$usr['id'] = $us['id_usr'];
        $usr['infGeneral'] = $us['infGeneral_ext'];
        $usr['imgUsser'] = $us['imgUsser_ext'];
        $usr['googleP'] = $us['googleP_ext'];
        $usr['twitter'] = $us['twitter_ext'];
        $usr['facebook'] = $us['facebook_ext'];
		return $usr;
	}

    /**
     * Verifica existencia de usuario apartir de usuario y contraseña
     *
     * @method boolean usserPassExist()
     * @access public
     * @param string $usser
     * @param string $password
     * @return boolean
     */
    public function usserPassExist($usser, $password)
	{
		
		$stmt = $this->_db->prepare(self::$usserPassValidate);
        $stmt->bindValue(':usser', $usser);
		$stmt->bindValue(':password', md5($password));
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
		return $exist['exist'];
	}

    /**
     * Cambia contraseña de usuario
     *
     * @method boolean passSet()
     * @access public
     * @param string $usser
     * @param string $password
     * @return boolean
     */
    public function passSet($usser, $password)
    {
        $stmt = $this->_db->prepare(self::$passSet);
        $stmt->bindValue(':usser', $usser);
        $stmt->bindValue(':password', md5($password));
        return $stmt->execute();
    }

    /**
     * Trae información de usuario por nombre de usuario
     *
     * @method array usserUsserGet
     * @access public
     * @param string $usser
     * @return array
     */
    public function usserUsserGet($usser)
    {
        $stmt = $this->_db->prepare(self::$usserUsserGet);
        $stmt->bindValue(':usser', $usser);
        $stmt->execute();
        return $this->_functions->fetchUnset($stmt->fetch());
    }

    /**
     * Cambia nombre
     *
     * @method boolean usserNameSet()
     * @access public
     * @param string $idUsr
     * @param string $name
     * @return boolean
     */
    public function usserNameSet($idUsr, $name)
    {
        $stmt = $this->_db->prepare(self::$usserNameSet);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':idUsr', $idUsr);
        return $stmt->execute();
    }

    /**
     * Cambia apellido
     *
     * @method boolean lastNameSet()
     * @access public
     * @param string $idUsr
     * @param string $lastName
     * @return boolean
     */
    public function lastNameSet($idUsr, $lastName)
    {
        $stmt = $this->_db->prepare(self::$lastNameSet);
        $stmt->bindValue(':lastName', $lastName);
        $stmt->bindValue(':idUsr', $idUsr);
        return $stmt->execute();
    }

    /**
     * Cambia fecha de nacimiento
     *
     * @method boolean birthdaySet()
     * @access public
     * @param string $idUsr
     * @param string $birthday
     * @return boolean
     */
    public function birthdaySet($idUsr, $birthday)
    {
        $stmt = $this->_db->prepare(self::$birthdaySet);
        $stmt->bindValue(':birthday', $birthday);
        $stmt->bindValue(':idUsr', $idUsr);
        return $stmt->execute();
    }

    /**
     * Cambia sexo de usuario
     *
     * @method boolean sexSet()
     * @access public
     * @param string $idUsr
     * @param string $sex
     * @return boolean
     */
    public function sexSet($idUsr, $sex)
    {
        $stmt = $this->_db->prepare(self::$sexSet);
        $stmt->bindValue(':sex', $sex);
        $stmt->bindValue(':idUsr', $idUsr);
        return $stmt->execute();
    }

    /**
     * Cambia google+ de usuario
     *
     * @method boolean gooleUsserSet()
     * @access public
     * @param string $idUsr
     * @param string $google
     * @return boolean
     */
    public function googleUsserSet($idUsr, $google)
    {
        $stmt = $this->_db->prepare(self::$googleUsserSet);
        $stmt->bindValue(':google', $google);
        $stmt->bindValue(':idUsr', $idUsr);
        return $stmt->execute();
    }

    /**
     * Verifica existencia de información extra de usuario
     *
     * @method boolean usserExtraExist()
     * @access public
     * @param string $usser
     * @return boolean
     */
    public function usserExtraExist($usser)
    {
        $stmt = $this->_db->prepare(self::$usserExtraExist);
		$stmt->bindValue(':usser', $usser);
        $stmt->execute();
        $exist = $this->_functions->fetchUnset($stmt->fetch());
		return $exist['exist'];
    }

    /**
     * Inserta googole+ de usuario
     *
     * @method boolean googleUsserInsert()
     * @access public
     * @param string $usser
     * @param string $google
     * @return boolean
     */
    public function googleUsserInsert($usser, $google)
    {
        $stmt = $this->_db->prepare(self::$googleUsserInsert);
        $stmt->bindValue(':google', $google);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Inserta twitter de usuario
     *
     * @method boolean twitterUsserInsert()
     * @access public
     * @param string $usser
     * @param string $twitter
     * @return boolean
     */
    public function twitterUsserInsert($usser, $twitter)
    {
        $stmt = $this->_db->prepare(self::$twitterUsserInsert);
        $stmt->bindValue(':twitter', $twitter);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Cambia twitter de usuario
     *
     * @method boolean twitterUsserSeT()
     * @access public
     * @param string $usser
     * @param string $twitter
     * @return boolean
     */
    public function twitterUsserSet($usser, $twitter)
    {
        $stmt = $this->_db->prepare(self::$twitterUsserSet);
        $stmt->bindValue(':twitter', $twitter);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Inserta facebook de usuario
     *
     * @method boolean facebookInsert()
     * @access public
     * @param string $usser
     * @param string $facebook
     * @return boolean
     */
    public function facebookInsert($usser, $facebook)
    {
        $stmt = $this->_db->prepare(self::$facebookInsert);
        $stmt->bindValue(':facebook', $facebook);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Cambia facebook
     *
     * @method boolean facebookSet()
     * @access public
     * @param string $usser
     * @param string $facebook
     * @return boolean
     */
    public function facebookSet($usser, $facebook)
    {
        $stmt = $this->_db->prepare(self::$facebookSet);
        $stmt->bindValue(':facebook', $facebook);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Inserta biografía de usuario
     *
     * @method boolean usserBiographyInsert()
     * @access public
     * @param string $usser
     * @param string $biography
     * @return boolean
     */
    public function usserBiographyInsert($usser, $biography)
    {
        $stmt = $this->_db->prepare(self::$usserBiographyInsert);
        $stmt->bindValue(':biography', $biography);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }

    /**
     * Cambia biografia de usuario
     *
     * @method boolean biographySet
     * @access public
     * @param string $usser
     * @param string $biography
     * @return boolean
     */
    public function biographySet($usser, $biography)
    {
        $stmt = $this->_db->prepare(self::$biographySet);
        $stmt->bindValue(':biography', $biography);
        $stmt->bindValue(':usser', $usser);
        return $stmt->execute();
    }
}