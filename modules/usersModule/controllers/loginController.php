<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Controllers;

class loginController extends \Core\Controller
{
	public function __construct()
    {
		parent::__construct();
    }

	public function index() {}

	/**
	 * Loguea al usuario
	 *
	 * @method boolean login()
	 * @access public
	 * @return boolean
	 */
    public function login($post = FALSE)
    {
        if( !isset($post) OR !is_array($post))
        {
			echo "Acá andamos 1";
            $_SESSION['errors']->errorAdd('Se esperaba un array $post : '.__FILE__.' ('.__LINE__.')');
			return false;
        }

        if( !isset($post['user']) OR $post['user'] == '' )
        {
            $_SESSION['errors']->errorAdd('No está definido $post[\'user\'] : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $usser = $post['user'];
		if( !$this->_functions->expressionValidate('usser', $usser) )
		{
			$_SESSION['errors']->errorAdd($usser.' No es un usuario valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

        if( !isset($post['contrasenia']) OR $post['contrasenia'] == '' )
        {
            $_SESSION['errors']->errorAdd('No está definido $post[\'contrasenia\'] : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $password = $post['contrasenia'];
        $usser = $this->_model->usserGet($usser, $password);
        if($usser == '')
        {
            return false;
        } elseif($usser['active'] == 0) {
            return 2;
        } else {
			if( isset($_SESSION['profileUSR']) )
			{
				unset($_SESSION['profileUSR']);
			}
            \Core\Session::set('nameUSR', $usser['name']);
			\Core\Session::set('lastNameUSR', $usser['lastName']);
            \Core\Session::set('birthdateUSR', $usser['birthday']);
            \Core\Session::set('registerDateUSR', $usser['registration']);
            \Core\Session::set('regionUSR', $usser['region']);
            \Core\Session::set('sexUSR', $usser['sex']);
            \Core\Session::set('usserUSR', $usser['usser']);
            \Core\Session::set('mailUSR', $usser['mail']);
            \Core\Session::set('profileUSR', $usser['profile']);
			\Core\Session::set('activeUSR', $usser['active']);
            \Core\Session::set('idUSR', $usser['id']);
			\Core\Session::set('generalInfoUSR', $usser['infGeneral']);
			\Core\Session::set('imgUSR', $usser['imgUsser']);
			\Core\Session::set('googleUSR', $usser['googleP']);
			\Core\Session::set('twitterUSR', $usser['twitter']);
			\Core\Session::set('facebookUSR', $usser['facebook']);
            \Core\Session::set('browserUSR', $_SERVER['HTTP_USER_AGENT']);
            \Core\Session::set('ipUSR', $this->_functions->ipGet());
            \Core\Session::set('isAuthUSR', \Core\Session::issAuth());
            if($_SESSION['isAuthUSR'])
            {
                return 3;
            } else {
                return false;
            }
            return 3;
        }
        return false;
    }

	/**
	 * Cierra sesion de usuario
	 *
	 * @method boolean logout()
	 * @access public
	 * @return boolean
	 */
	public function logout()
	{
		unset($_SESSION['nameUSR']);
		unset($_SESSION['lastNameUSR']);
		unset($_SESSION['birthdateUSR']);
		unset($_SESSION['registerDateUSR']);
		unset($_SESSION['regionUSR']);
		unset($_SESSION['sexUSR']);
		unset($_SESSION['usserUSR']);
		unset($_SESSION['mailUSR']);
		$_SESSION['profileUSR'] = DEFAULT_PROFILE;
		unset($_SESSION['activeUSR']);
		unset($_SESSION['idUSR']);
		unset($_SESSION['generalInfoUSR']);
		unset($_SESSION['imgUSR']);
		unset($_SESSION['googleUSR']);
		unset($_SESSION['twitterUSR']);
		unset($_SESSION['facebookUSR']);
		unset($_SESSION['browserUSR']);
		unset($_SESSION['ipUSR']);
		unset($_SESSION['isAuthUSR']);
		return true;
	}
}