<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Controllers;

class changeController extends \Core\Controller
{
    public function __construct()
    {
		parent::__construct();
    }

	public function index() {}

	/**
	 * Cambia contraseña de usuario
	 *
	 * @method boolean passChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
    public function passChange($post = FALSE)
    {
        if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        if( !isset($post['c_anterior']) OR $post['c_anterior'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'c_anterior\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $passPrevious = $post['c_anterior'];

        if($this->_model->usserPassExist(\Core\Session::get('idUSR'), $passPrevious) != 1)
        {
            $_SESSION['errors']->errorAdd('No coinciden las contraseñas : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        if( !isset($post['contrasenia_confirmation']) OR $post['contrasenia_confirmation'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'contrasenia_confirmation\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $password = $post['contrasenia_confirmation'];

        if( !isset($post['contrasenia']) OR $post['contrasenia'] == '' )
        {
            $_SESSION['errors']->errorAdd('$post[\'contrasenia\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $confirmation = $post['contrasenia'];

        if( $confirmation != $password)
        {
            $_SESSION['errors']->errorAdd('Las contraseñas no coinciden : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        if($this->_model->passSet(\Core\Session::get('idUSR'), $password))
        {
			$this->_functions->mailSend([
				'Address'	=>	\Core\Session::get('mailUSR'),
				'Subject'	=>	'Cambio de contraseña',
				'Body'	=>	'Se ha cambiado tu contraseña, ahora es:  '.$password
			]);
            $_SESSION['errors']->otherAdd('La contraseñas se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
            return true;
        } else {
            $_SESSION['errors']->errorAdd('Error al actualizar la contraseña : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
    }

	/**
	 * Cambia nombre de usuario
	 *
	 * @method boolean nameChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function nameChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['nameUSR']) OR $post['nameUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'nameUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $currentName = $post['nameUSR'];

		if($this->_functions->expressionValidate('alpha', $currentName))
		{
			if($this->_model->usserNameSet(\Core\Session::get('idUSR'), $currentName))
			{
				$_SESSION['errors']->otherAdd('El nombre se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['nameUSR'] = $currentName;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el nombre : '.__FILE__.' ('.__LINE__.')');
			return false;
			}
		} else {
			$_SESSION['errors']->errorAdd($currentName.' no es un nombre valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
	}

	/**
	 * Cambia apellido de usuario
	 *
	 * @method boolean lastNameChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function lastNameChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['lastNameUSR']) OR $post['lastNameUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'lastNameUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $currentName = $post['lastNameUSR'];

		if($this->_functions->expressionValidate('alpha', $currentName))
		{
			if($this->_model->lastNameSet(\Core\Session::get('idUSR'), $currentName))
			{
				$_SESSION['errors']->otherAdd('El apellido se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['lastNameUSR'] = $currentName;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el apellido : '.__FILE__.' ('.__LINE__.')');
			return false;
			}
		} else {
			$_SESSION['errors']->errorAdd($currentName.' no es un apellido valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
	}

	/**
	 * Cambia fecha de nacimiento de usuario
	 *
	 * @method boolean birthdayChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function birthdayChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['birthdateUSR']) OR $post['birthdateUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'birthdateUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $birthday = $post['birthdateUSR'];

		if($this->_functions->expressionValidate('date', $birthday))
		{
			if($this->_model->birthdaySet(\Core\Session::get('idUSR'), $birthday))
			{
				$_SESSION['errors']->otherAdd('La fecha de nacimiento se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['birthdateUSR'] = $birthday;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar la fecha de nacimiento : '.__FILE__.' ('.__LINE__.')');
			return false;
			}
		} else {
			$_SESSION['errors']->errorAdd($birthday.' no es una fecha de nacimiento valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
	}

	/**
	 * Cambia sexo de usuario
	 *
	 * @method boolean sexChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function sexChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['sexUSR']) OR $post['sexUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'sexUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $sex = $post['sexUSR'];

		if($this->_model->sexExist($sex) == 1)
		{
			if($this->_model->sexSet(\Core\Session::get('idUSR'), $sex))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['sexUSR'] = $sex;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
		return false;
	}

	/**
	 * Cambia google+ de usuario
	 *
	 * @method boolean googleChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function googleChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['googleUSR']) OR $post['googleUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'googleUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $google = $post['googleUSR'];

		if($this->_model->usserExtraExist( \Core\Session::get('idUSR') ) == 0 )
		{
			if($this->_model->googleUsserInsert(\Core\Session::get('idUSR'), $google))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['googleUSR'] = $google;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		} else {
			if($this->_model->googleUsserSet(\Core\Session::get('idUSR'), $google))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['googleUSR'] = $google;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
	}

	/**
	 * Cambia twitter de usuario
	 *
	 * @method boolean twitterChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function twitterChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['twitterUSR']) OR $post['twitterUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'twitterUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $twitter = $post['twitterUSR'];

		if($this->_model->usserExtraExist( \Core\Session::get('idUSR') ) == 0 )
		{
			if($this->_model->twitterUsserInsert(\Core\Session::get('idUSR'), $twitter))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['twitterUSR'] = $twitter;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		} else {
			if($this->_model->twitterUsserSet(\Core\Session::get('idUSR'), $twitter))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['twitterUSR'] = $twitter;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
	}

	/**
	 * Cambia Facebook de usuarios
	 *
	 * @method boolean facebookInsert()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function facebookInsert($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['facebookUSR']) OR $post['facebookUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'facebookUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $facebook = $post['facebookUSR'];

		if($this->_model->usserExtraExist( \Core\Session::get('idUSR') ) == 0 )
		{
			if($this->_model->facebookInsert(\Core\Session::get('idUSR'), $facebook))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['facebookUSR'] = $facebook;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		} else {
			if($this->_model->facebookSet(\Core\Session::get('idUSR'), $facebook))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['facebookUSR'] = $facebook;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
	}

	/**
	 * Cambia biografia de usuario
	 *
	 * @method boolean biographyChange()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function biographyChange($post = FALSE)
	{
		if( !isset($post) OR !is_array($post))
        {
            $_SESSION['errors']->errorAdd('$post no está definido o no es array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

		if( !isset($post['biografiaUSR']) OR $post['biografiaUSR'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'biografiaUSR\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $biography = $post['biografiaUSR'];

		if($this->_model->usserExtraExist( \Core\Session::get('idUSR') ) == 0 )
		{
			if($this->_model->usserBiographyInsert(\Core\Session::get('idUSR'), $biography))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['generalInfoUSR'] = $biography;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		} else {
			if($this->_model->biographySet(\Core\Session::get('idUSR'), $biography))
			{
				$_SESSION['errors']->otherAdd('El sexo se cambió correctamente : '.__FILE__.' ('.__LINE__.')');
				$_SESSION['generalInfoUSR'] = $biography;
				return true;
			} else {
				$_SESSION['errors']->errorAdd('Ocurrio un error al registrar el sexo : '.__FILE__.' ('.__LINE__.')');
				return false;
			}
		}
	}
}