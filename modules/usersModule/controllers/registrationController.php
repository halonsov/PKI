<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Controllers;

class registrationController extends \Core\Controller
{
	public function __construct()
    {
		parent::__construct();
    }

	public function index() {}

	/**
	 * Obtiene los sexos
	 *
	 * @method string sexOptionGet()
	 * @access public
	 * @return string
	 */
    public function sexsOptionGet()
    {
        return $this->_functions->arrayOptionSet($this->_model->sexGet());
    }

	/**
	 * Obtiene los paises
	 *
	 * @method string countriesOptionGet()
	 * @access public
	 * @return string
	 */
    public function countriesOptionGet()
    {
        return $this->_functions->arrayOptionSet($this->_model->countriesGet());
    }

	/**
	 * Registra usuario
	 *
	 * @method boolean countriesOptionGet()
	 * @access public
	 * @param array $post
	 * @return boolean
	 */
	public function ussersRegistration($post = FALSE)
	{
		if( !is_array($post) )
		{
			$_SESSION['errors']->errorAdd('Se esperaba un array $post : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['name']) OR $post['name'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'name\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$name = $post['name'];
		if( !$this->_functions->expressionValidate('alpha', $name) )
		{
			$_SESSION['errors']->errorAdd($name.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['lastName']) OR $post['lastName'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'lastName\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$lastName = $post['lastName'];
		if( !$this->_functions->expressionValidate('alpha', $lastName) )
		{
			$_SESSION['errors']->errorAdd($lastName.' no cumple la expresión regular : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['paisselect']) OR $post['paisselect'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'paisselect\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$country = $post['paisselect'];
		if( $this->_model->countryExist($country) == 0 )
		{
			$_SESSION['errors']->errorAdd('No existe el pais '.$country.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['ciudadselect']) OR $post['ciudadselect'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'ciudadselect\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$city = $post['ciudadselect'];
		if( $this->_model->cityExist($city) == 0 )
		{
			$_SESSION['errors']->errorAdd('No existe la ciudad '.$city.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['regionselect']) OR $post['regionselect'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'regionselect\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$region = $post['regionselect'];
		if( $this->_model->regionExist($region) == 0 )
		{
			$_SESSION['errors']->errorAdd('No existe la región '.$region.' : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['fnacimiento']) OR $post['fnacimiento'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'fnacimiento\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$birthday = $post['fnacimiento'];
		if( !$this->_functions->expressionValidate('date', $birthday) )
		{
			$_SESSION['errors']->errorAdd($birthday.' No es una fecha valida : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['user']) OR $post['user'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'user\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$usser = $post['user'];
		if( !$this->_functions->expressionValidate('usser', $usser) )
		{
			$_SESSION['errors']->errorAdd($usser.' No es un usuario valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		if( $this->_model->usserNameExist($usser) )
		{
			$_SESSION['errors']->errorAdd('Usuario '.$usser.' Ya existe en la base de datos : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['correo']) OR $post['correo'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'correo\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$mail = $post['correo'];
		if( !$this->_functions->expressionValidate('mail', $mail) )
		{
			$_SESSION['errors']->errorAdd($mail.' No es un mail valido : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		if($this->_model->usserMailExist($mail))
		{
			$_SESSION['errors']->errorAdd('Correo '.$mail.' Ya existe en la base de datos : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		if( !isset($post['contrasenia_confirmation']) OR $post['contrasenia_confirmation'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'contrasenia_confirmation\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$password = $post['contrasenia_confirmation'];

		if( !isset($post['contrasenia']) OR $post['contrasenia'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'contrasenia\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$passwordC = $post['contrasenia'];

		if($password != $passwordC)
		{
			$_SESSION['errors']->errorAdd('No coinciden las contraseñas : '.__FILE__.' ('.__LINE__.')');
			return false;
		}


		if( !isset($post['sexsselect']) OR $post['sexsselect'] == '' )
		{
			$_SESSION['errors']->errorAdd('No está definido, o es nulo $post[\'sexsselect\'] : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
		$sex = $post['sexsselect'];
		if( !$this->_model->sexExist($sex) )
		{
			$_SESSION['errors']->errorAdd('Sexo '.$sex.' No existe en la base de datos : '.__FILE__.' ('.__LINE__.')');
			return false;
		}

		$last_id = $this->_model->usserSet([
				'name' 		=>	$name,
				'lastName'		=>	$lastName,
				'birthdate'	=>	$birthday,
				'region'		=>	$region,
				'sex'			=>	$sex,
				'usser'		=>	$usser,
				'password'	=>	$password,
				'mail'		=>	$mail,
				'active'		=>	0,
				'profile'		=>	2
			]);
		if( $last_id )
		{
			$this->_functions->mailSend([
				'Address'	=>	$mail,
				'Subject'	=>	'registro exitoso',
				'Body'	=>	'Te has registrado tu cuenta esá desactivada, para activar tu cuenta visita '.SECURE_BASE_URL.'users/active/'.$last_id.'/'.md5(md5(md5(md5($usser))).md5(md5(md5(md5(md5($password))))).md5(md5(md5(md5(md5(md5(md5($name.' '.$lastName))))))))
			]);
			return true;
		} else {
			$_SESSION['errors']->errorAdd('Ocurrió un error al intentar registrar el usuario en la base de datos : '.__FILE__.' ('.__LINE__.')');
			return false;
		}
	}
}