<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Controllers;

class recoverController extends \Core\Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {}

    /**
     * Recupera información del usuario
     *
     * @method boolean recover()
     * @access public
     * @param array $post
     * @return boolean
     */
    public function recover($post = FALSE)
    {
        if( !isset($post) OR !is_array($post) )
        {
            $_SESSION['errors']->errorAdd('$pos no está definido o no es un array : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        if( !isset($post['campo']) OR $post['campo'] == '')
        {
            $_SESSION['errors']->errorAdd('$post[\'campo\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $field = $post['campo'];

        if($field != 1 AND $field !=2 )
        {
            $_SESSION['errors']->errorAdd('$field = '.$field.' no es valido : '.__FILE__.' ('.__LINE__.')');
            return false;
        }

        if( !isset($post['valor']) OR $post['valor'] == '' )
        {
            $_SESSION['errors']->errorAdd('$post[\'valor\'] no está definido o es nulo : '.__FILE__.' ('.__LINE__.')');
            return false;
        }
        $value = $post['valor'];

        if($field == 1)
        {
            if($this->_model->usserMailExist($value) == 1)
            {
                
            } else {
                
            }
        } elseif($field == 2) {
            if($this->_model->usserNameExist($value) == 1)
            {
                $usuario = $this->_model->usserUsserGet($value);
                $password = bin2hex(openssl_random_pseudo_bytes(10));
                if($this->_model->passSet($usuario['id_usr'], $password))
                {
                    $this->_functions->mailSend([
                        'Address'	=>	$usuario['mail_usr'],
                        'Subject'	=>	'Tus datos',
                        'Body'	=>	'Tus datos son:
Nombre de usuario : '.$value.'
Correo: '.$usuario['mail_usr'].'
Contraseña : '.$password
                    ]);
                    return true;
                }
            } else {
                return false;
            }
        }
    }
}