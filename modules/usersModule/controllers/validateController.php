<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

namespace usersModule\Controllers;

class validateController extends \Core\Controller
{
    public function __construct()
    {
		parent::__construct();
    }

    public function index() {}

	/**
	 * Verifica existencia de usuario
	 *
	 * @method boolean usserExist()
	 * @access public
	 * @param string $usser
	 * @return boolean
	 */
    public function usserExist($usser = FALSE)
    {
        if( !isset($usser) OR $usser == '')
        {
            $_SESSION['errors']->errorAdd('No está definida o es nula la variable $usser : '.__FILE__.' ('.__LINE__.')');
            return false;
        } else {
            $_SESSION['errors']->otherAdd('Existe $usser : '.__FILE__.' ('.__LINE__.')');
            return $this->_model->usserIdExist($usser);
        }
    }

	/**
	 * Regresa información de usuario a partir de id
	 *
	 * @method array usserIdGet()
	 * @access public
	 * @param string $id
	 * @return array
	 */
	public function usserIdGet($id = FALSE)
	{
		if( !isset($id) OR $id == '')
		{
			return FALSE;
		}

		return $this->_model->usserIdGet($id);
	}
	
	/**
	 * Activa usuario
	 *
	 * @method boolean usserActivate()
	 * @access public
	 * @param string $usser
	 * @return boolean
	 */
    public function usserActive($usser = FALSE)
    {
        if( !isset($usser) OR $usser == '')
        {
            $_SESSION['errors']->errorAdd('No está definida o es nula la variable $usser : '.__FILE__.' ('.__LINE__.')');
            return false;
        } else {
            $_SESSION['errors']->otherAdd('Activando $usser : '.__FILE__.' ('.__LINE__.')');
            return $this->_model->usserActive($usser);
        }
    }
}