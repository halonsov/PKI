<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", realpath(dirname(__FILE__)) . DS);
define("CORE_DIR", ROOT . "core" . DS);

require_once CORE_DIR."Config.php";
require_once CORE_DIR.'Functions.php';
require_once CORE_DIR."Session.php";
require_once CORE_DIR."Errors.php";
require_once CORE_DIR."Request.php";
require_once CORE_DIR."DataBase.php";
require_once CORE_DIR."Model.php";
require_once CORE_DIR."View.php";
require_once CORE_DIR."Controller.php";
require_once CORE_DIR."Module.php";
require_once CORE_DIR."AutoLoad.php";
require_once LIB_DIR."phpmailer/class.phpmailer.php";

\Core\Session::init();
if( !isset($_SESSION['profileUSR']) )
{
	$_SESSION['profileUSR'] = DEFAULT_PROFILE;
}
if( !isset($_COOKIE['menuActive']) )
{
	setcookie('menuActive', 2);
}
$GLOBALS['ERRORS'] = 'error';

$_SESSION['errors'] = new \Core\Errors();
$_SESSION['errors']->levetSet(3);
$modules = new \Core\Model();
$_SESSION['permissions'] = $modules->modulesPermissionsGet();
unset($modules);
$_SESSION['errors']->otherAdd('Creando objeto \Core\Autoload : '.__FILE__.' ('.__LINE__.')');
$return = new \Core\AutoLoad();
$_SESSION['errors']->otherAdd('LLamando al método \Core\Autoload::run() : '.__FILE__.' ('.__LINE__.')');
if(!$return->run())
{
	$_SESSION['errors']->errorAdd('Error al llamar al metodo \Core\AutoLoad::run() :'.__FILE__.' ('.__LINE__.')');
}