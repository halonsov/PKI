function usserExist(field, value)
{
    if($(value).val() != '' && field != '')
    {
        var usser = $(value).val();
        $.ajax({
            url: "/ajax/usserExist/"+field+"/"+usser,
            success: function(data) {
                if (data == 1)
                {
                    $(value).val('');
                    $(value).attr('style', 'border-color: red; color: red;');
                    if (field == 'usuario')
                    {
                        $(value).attr('placeholder', 'Usuario no disponible');
                    } else if (field == 'correo') {
                        $(value).attr('placeholder', 'Correo no disponible');
                    }
                } else if (data == 1) {
                    $(value).attr('style', 'border-color: #000000; color: #000000;');
                }
            }
        });
    }
}

function usserValidate(field, value)
{
    if ($(value).val() != '' && field != '')
    {
        var usser = $(value).val();
        if (field == 'contrasenia')
        {
            $.ajax({
                url: "/ajax/usserValidate/"+field+"/"+usser,
                success: function(data) {
                    if (data == 0)
                    {
                        $(value).val('');
                        $(value).attr('style', 'border-color: red; color: red;');
                        $(value).attr('placeholder', 'Contraseña incorrecta');
                    } else if (data == 1) {
                        $(value).attr('style', 'border-color: #000000; color: #000000;');
                    }
                }
            });
        } else if (field == 'usuario' || field == 'correo') {
            $.ajax({
            url: "/ajax/usserExist/"+field+"/"+usser,
            success: function(data) {
                if (data == 0)
                {
                    $(value).val('');
                    $(value).attr('style', 'border-color: red; color: red;');
                    if (field == 'usuario')
                    {
                        $(value).attr('placeholder', 'Usuario no existe');
                    } else if (field == 'correo') {
                        $(value).attr('placeholder', 'Correo no existe');
                    }
                } else if (data == 1) {
                    $(value).attr('style', 'border-color: #000000; color: #000000;');
                }
            }
        });
        }
    }
}