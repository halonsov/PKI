<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 *
 *
 * @author HAV<halonsov@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html
 * @category Core
 */
?>
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta name="generator" content="PKI 0.0.1" />
        <meta name="title" content="PKI" />
        <meta name="description" content="Pki ++" />
        <meta name="keywords" content="pki" />
        <title><?= ( isset($this->pageTitle) )? $this->pageTitle : DEFAULT_TITLE ;?></title>
        <?php
        foreach(explode(',', DEFAULT_CSS_SCRIPTS) AS $value)
        { ?>
            <link rel="stylesheet" href="/media/css/<?= $value ?>" type="text/css" />
        <?php } ?>
        
        <?php
        foreach(explode(',',  DEFAULT_JS_SCRIPTS) AS $value)
        { ?>
            <script src="<?= $value ?>" type="text/javascript" ></script>
        <?php } ?>
        <script>
            var submenu = [
                ''
                <?php foreach($this->menu AS $key => $value)
                { ?>
                    <?php if(isset($value['submenu']))
                    { ?>
                        ,[
                            <?php foreach($value['submenu'] AS $value1)
                            { ?>
                                ["<?= $value1['link'] ?>", "<?= $value1['menu'] ?>"],
                            <?php } ?>
                        ]
                    <?php } else { ?>
                        ,''
                    <?php }
                } ?>
            ];
            function menuActive(menu)
            {
                $('li').removeClass('active');
                $('#tMenu'+menu).addClass('active');
                document.cookie="menuActive="+menu;
            }
        </script>
        <?= $this->parameterGet(); ?>
        <meta name="description" content="PKI">
    </head>
    <body class="hide-extras">
        <div class="off-canvas-wrap">
            <div class="inner-wrap">
                <nav class="tab-bar show-for-small">
                    <a class="left-off-canvas-toggle menu-icon">
                        <span>Menú</span>
                    </a>
                </nav>
                <aside class="left-off-canvas-menu">
                    <ul class="off-canvas-list">
                        <?php
                        foreach($this->menu AS $value)
                        { ?>
                            <li class="has-dropdown not-click">
                                <label><?= $value['menu'] ?></label>
                                <ul class="dropdown">
                                    <li>
                                        <a href="<?= $value['link'] ?>"><?= $value['menu'] ?></a>
                                    </li>
                                    <?php
                                    if(isset($value['submenu']))
                                    {
                                        foreach($value['submenu'] AS $value1)
                                        { ?>
                                            <li>
                                                <a href="<?= $value1['link'] ?>"><?= $value1['menu'] ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </aside>
                <section class="scroll-container" role="main">
                    <div class="row">
                        <div class="small-12 large-12 columns noPadding">
                            <div class="small-12 large-12 columns noPadding sticky">
                                <nav class="top-bar hide-for-small-only" data-topbar data-options="sticky_on: large">
                                    <ul class="title-area">
                                        <li class="name">
                                            <h1>
                                                <a href="/">PKI</a>
                                            </h1>
                                        </li>
                                    </ul>
                                    <section class="top-bar-section">
                                        <ul class="right">
                                            <?php
                                            if( \Core\Session::issAuth() )
                                            {?>
                                                <li class='has-dropdown'>
                                                    <a href="#"><?= $_SESSION['usserUSR'] ?></a>
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="/users/profile">Perfil</a>
                                                        </li>
                                                        <li>
                                                            <a href="/users/logout">logout</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="divider"></li>
                                                <?php if( !isset($_SESSION['imageUSR']))
                                                { ?>
                                                    <li>
                                                        <img class='usserImage' src='<?= DEFAULT_USSER_IMAGE ?>'>
                                                    </li>
                                                <?php } else { ?>
                                                    <li>
                                                        <img class='usserImage' src='<?= $_SESSION['imageUSR'] ?>'>
                                                    </li>
                                                <?php }
                                            } ?>
                                        </ul>
                                        <ul class="left">
                                            <?php
                                            $i=1;
                                            foreach($this->menu AS $value)
                                            { ?>
                                                <li id="tMenu<?= $i ?>" class="
                                                    <?php if( isset($_COOKIE['menuActive']) )
                                                    {
                                                        $active = $_COOKIE['menuActive'];
                                                    } else {
                                                        echo $active = DEFAULT_MENU;
                                                    }?>
                                                    <?php echo ($i == $active)? 'active': '' ?>
                                                    <?php echo ( isset($value['submenu']) AND count($value['submenu']) >= 1)? 'has-dropdown' : '' ?>
                                                ">
                                                    <a id="" href="<?= $value['link'] ?>" onclick='menuActive(<?= $i ?>)'><?= $value['menu'] ?></a>
                                                    <?php if( isset($value['submenu']) AND count($value['submenu']) >= 1)
                                                    { ?>
                                                        <ul class="dropdown">
                                                            <?php foreach($value['submenu'] AS $value2)
                                                            { ?>
                                                                <li>
                                                                    <a href="<?= $value2['link'] ?>" onclick='menuActive(<?= $i ?>)'><?= $value2['menu'] ?></a>
                                                                </li>
                                                                <li class="divider"></li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                                <li class="divider"></li>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </ul>
                                    </section>
                                </nav>
                            </div>
                            <div class="small-12 large-10 columns">
                                <div class="large-4 large-centered columns">
                                    <?= $this->messageGet() ?>
                                </div>
                            </div>
                            <div class="small-12 large-12 columns noPadding">
                                <div class="small-12 large-9 large-centered columns noPadding contenido">